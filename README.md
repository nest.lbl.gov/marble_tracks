The [marble_tracks](https://gitlab.com/nest.lbl.gov/marble_tracks) package contains a collection of python modules that can be used to create a workflow.

# Concepts

The [concepts](concepts.md) used with create these modules are explained [here](concepts.md).
