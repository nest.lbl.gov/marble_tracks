# Concepts used to develop Marble Tracks

The documents provided here give a walk-through of the concepts used to develop `marble_tracks`.

The order in which these concepts are expected to be covered are:

1. Flow of Control
1. Data Handling
1. External Implementations
1. Combining Control, Data and Externals
1. Defining a Workflow
1. Threading
1. Termination
1. "Arrayed" Actions
1. Checkpointing

* [Flow of Control](concepts/tracks.md) : This introduces the elements that make up the flow of control through a workflow.

    Demonstrations:

    * `marble_concept_1` : Runs `start_end.py` that is a simple workflow with a start node connected to an end node.

    * `marble_concept_2` : Runs `single_trap.py` that is workflow with a start node connected to and action node that i s, in turn, conected an end node.

    * `marble_concept_3` : Runs `exclusive_trap.py` that is workflow demonstrating "exclusive" diverging and converging nodes.

    * `marble_concept_4` : Runs `parallel_traps.py` that is workflow demonstrating "parallel" diverging and converging nodes.


* [Data Handling](concepts/trays.md) : This introduces the elements that use to access and updated data within a workflow.

    Demonstrations:

    * `marble_concept_5` : Runs `action_data.py` that demonstrates how data is delivered to and received from a node.

    * `marble_concept_6` : Runs `guard_data.py` that demonstrates how data is delivered to a conditionally expressions the decides routing through a workflow.


* [External Implementations](concepts/externals.md) : This introduces the mechanics that enable external implementation of workflow tasks.

    Demonstrations:

    * None


* [Combining Control, Data and Externals](concepts/combined.md) : This explains how the elements described in the previous three topics can be combined to create a functioning workflow.

    Demonstrations:

    * `marble_concept_7` : Runs `data_traps.py` that re-works the `exclusive_trap.py` to include data handling to nodes and calls out to actions and conditional expressions.

* [Defining a Workflow](concepts/definition.md) : This introduces how to workflow a workflow as an ensomble of the the previously discussed elements.

    Demonstrations:

    * `marble_concept_8` : Runs `toml_traps.py` that re-works `data_traps.py` to build the workflow from its description.

