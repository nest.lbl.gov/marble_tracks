# Array Traps

When a workflow needs to execute a fixed number of identical Actions Traps in parallel, this can be implemented as a parallel Diverging Trap feeding the necessary number of channels, each of which contain an Action Trap, and these, in turn, flow into a parallel Converging Trap. However if the number if not know when the workflow is being build this approach does not work as a structure defining a workflow is declarative, i.e. is can not be changed. In this case an Array Trap needs to be used.

Array Trays are a specialized try of Action Trap. Like the fixed structure discussed above, these traps can be seen as an encapsulation of a parallel Divergent Trap, feeding into a set of repetition the Trap itself, follow by a  parallel Converging Trap bringing everything back to a single flow of control. The difference is that the number of channels exiting the Diverging Trap is dynamic and is derived from the workflow data.

To clearly distinguish the channels exiting the implied Diverging Trap feeding the multiple copies of the Action Trap and entering the implied Converging trap, each connection for the Diverging to Converting Traps will be referred to as a Lane.


## Executing Multiple Actions

When a Action Trap needs to be treated as an Array Trap the number of Lanes needs to be specified. This can be done in one of two ways.

* The number of Lanes is explicitly stated by a variable in the workflow data; or
* The size of a collection in the workflow data is used to defined the number of Lanes.

This information is held in the WrappedAction, for reason that should become apparent below, and thus the `WrappedAction.get_lane_count` method is introduced that will return the number of Lanes that should be created, or -1 if this is a normal Action Trap.


The result of the `WrappedAction.get_lane_count` method can be used to determine how many time to perform this Action of the Trap. However to manage the flow of control correctly a new `Trap.perfrom_lane()` method is needed so that it only calls the `Trap.send_marble()` method when the Action from each Lanes has completed. This means that all invocations of this method need to share a thread safe counter that tracks the number of completed Lanes and only when all Lanes have completed should the `Trap.send_marble()` be invoked.


## Lane Specific Data

It its simplest form, an Array Trap runs a set of identical actions all with the same data. However there is also a use case where each Lane may run with its own specific data. In order to support this, it must be possible to for each Lane to distinguish itself from the others. This is done by giving each lane its own index starting at 0 and incrementing by 1 for each new Lane.

The natural way of making this index available to an Action is to create a Tray for each Lane that contains the index in named compartment and then a picker can be used to match it with the imput required by the Action. This leads to the addition of a `WrappedAction.perform_lane()` method that constructs a new Tray that wraps the existing one and into which the index is loaded with a specified compartment name. All of this is done before calling the  `WrappedAction.perform()` method.

Furthermore, in order to use the same action in both a normal Action Trap and an Array Trap, the Array Trap needs one more piece of behavior. If a collection is used to specify the number of Lanes, each item in that collection should be presented to a different lane. This behavior is simply an extension of what is done in the `WrappedAction.perform_lane()` method but instead of the index being placed in the new Tray, the contents of the collection at that index is placed in the Tray.


## Declaring an Array Trap

In order for a normal Action Trap to become an Array Trap the number of Lanes needs to be declared. As noted above, this can be done in one of two ways:

* The number of Lanes is explicitly stated by a variable in the workflow data; or
* The size of a collection in the workflow data is used.

In the first case a new item in the `<name>.control.action` table is added, the `distributor`. This item will contain the number of Lanes that need to be created.

In the other case above the `distributor` item will identity a `<name>.data.distibutor` entry that declares the mapping of a collection to a Lane scoped tray compartment. The declared mapping provides both the number of Lanes to be created, i.e. the size of the collection, and maps each element in the collection into its own Lane.

In addition to the `distributor` item, a `collector` item is also added to the `<name>.control.action` table. This identifies, when applicable, the `<name>.data.collector` entry that will collect the results of each Lane and merge them back together into a single collection.

    <name>                   # Parent
        data                 # Parent
            distibutor       # Array of Tables (optional, defines Distributors)
                id           # Value (unique within this array)
                source       # Value (reference to a tray element containing a collection)
                target       # Value (reference to a lane-scoped tray element)
            collector        # Array of Tables (optional, defines Distributors)
                id           # Value (unique within this array)
                source       # Value (reference to a lane-scoped tray element)
                target       # Value (reference to a tray element containing a collection)

    <name>                   # Parent
        control              # Parent
            action           # Array of Tables (optional)
                id           # Value (unique within this array)
                action       # Value (reference to an action element)
                pickers      # Array of values (optional, reference to picker elements)
                putters      # Array of values (optional, reference to putter elements)
                distributor  # Value (reference to an distributor element)
                collector    # Value (reference to an collector element)
