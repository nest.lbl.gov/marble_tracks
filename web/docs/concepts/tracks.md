# Basic Flow of Control

This document lays out the concepts behind how the flow of control is managed by `marble-tracks`. By "flow of control"", we mean the mechanics that dictate when a particular part of a workflow should execute and under what conditions.

## Marbles Analogy

The flow of control can be visualized by imagining a marble rolling down a track. The simplest form this can take is a simple channel connecting the "start", i.e. where a marble is placed, to the "end", i.e. where the marble is drop out of the channel. A new set  of channels, from here on referred to as a track, can be built for each execution of a workflow so that two different executions will not interfere with each other. The instructions for creating such track can be captured in a Diagram that describes the flow of control through a workflow.

If the workflow is going to be any other than its simplest form, i.e. it is going to perform actions when a marble reaches in specified point in the track, then the marble will need to trigger such an action when it reaches that point. Morevoer, the marble not only needs to trigger the action, but it must wait at that point, the action is complete. This can be visualized as there being a trap on channel in the form of an indentation in which the marble rests. Upon dropping into the indentation the action will start and once completed a pin can be used to push the marble out of the indentation so it can continue along the channel.

The tray described above, where the arrival of a marble will trigger that trap's action and completion lets it continue, is the simplest form of trap and will be referred as an "Action Trap". There are more complex traps that may have one or more channels converging to them or diverging out of them. These break down into two types:

* multiple incoming channels and one outgoing, referred to as an "Downstream Trap"
* one incoming and multiple outgoing channels, referred to as an "Upstream Trap"

In the cases of these traps, marbles may be added or removed from the track depending on the purpose of the trap. How this addition and removal is managed a major part of the flow of control and will be discussed in detail below.

In BPMN concepts FlowNodes can be build from the three type of traps mentioned above. Inbound marbles will be dealt with by a Downstream Trap, whose single outgoing channel leads to the single incoming channel of an Action Trap that performs the FlowNodes action, if any. Once the Action Trap releases the marble via its single outgoing channel it will arrive, via a single incoming channel, at an Upstream Trap that determines which, if any, the outgoing channels will receive marbles.

The rest of this document lays out the mechanics of the Action, Inbound and Upstream Traps. A separate document will discuss how BPMN entities can be build from these Traps and their mechanisms.


## A Diagram and Tracks

---
* The `Track` interface replaces `GraphTraversal` interface in `lazarus`.

---

As noted above, 'The instructions for creating such track can be captured in a Diagram'. This means that a Diagram can be used create a Track instance into which a marble can be introduced. Such a Track instance is created by the `Diagram.create_track()` method, and the `Track.start()` method creates the first marble and sets it in motion on its way through the track.

The relationship between a Diagram and a Track is the same, within Object Oriented Programming languages, as that between a Class definition and a Class instance. Here the Diagram holds all of the code used needed to navigate a marble through a workflow while the Track holds the data for a single instance of the workflow that is being executed, i.e. it manages the marble (or marbles) within that instance.

The details of Marble Management will be discussed [later](#marble-management).


## Channels

---
* The `Channel` interface replaces `ControlTransfer` interface in `lazarus`.
* The `Downstream` interface replaces `ControlFlowReceiver` interface in `lazarus`.
---

Before dealing with the Traps, it makes sense to consider the "Channel" that moves marbles from one Trap to another. The Upstream Trap only needs to know about its outgoing Channels, but not to where they lead. Therefore the Upstream Trap uses the `Channel.transfer_mable()` method to send the marble on its way. This method turn will use the `Downstream.receive_marble()` to pass the marble on to the next Trap.

It is now time to discuss what happens when a marble is received.

### Inbound Channels

---
* The `Trap` interface replaces `ControlOwner` interface in `lazarus`.
* The `Upstream` interface replaces `OutgoingTransfers` abstract class in `lazarus`.

---

Both Action and Upstream Traps have at most a single incoming channels, so when they receive a marble they can immediately perform their action, if any, by invoking the `Trap.perform_action()` and once that action has complete send out using the `Upstream.send_marble()` method that selects the outgoing channel and sents the marble on its way. On the other hand Downstream Traps may have zero, one or more incoming Channel and the collective behavior of these channels must be one from the following list.

* Zero incoming Channels implies that this is a "start" of a Track and that a new marble should be introduced and sent downstream through the single outgoing Channel.

* One incoming Channel simply requires the Downstream Trap to pass on any marble that arrives out through the single outgoing Channel.

* Multiple incoming Channels will have one of two behaviors:

  * Exclusive: When a marble is received via any incoming channel, it should be passed out through the single outgoing Channel. This can be visualized as all the channels converging just before the trap and then the trap acting as an Action Trap that has no action. This is the default behavior for multiple incoming Channels.

  * Parallel: Only when a marble has been received by every incoming Channel should a marble be placed in the single outgoing Channel. This can be visualized as marbles from all incoming channels are collected in a counterweighted basket and then when that basket contains the correct about of marbles, i.e. it weighs more that its counterweight, it places a new marble in the single outgoing Channel. The Trap should the dispose of those marbles in the basket


### Outbound Channels


Mirroring the case of incoming channels discussed above, the Action and Downstream Traps have at most one outgoing channel, whereas the Upstream Trap may have zero, one or more. In this case the collective behavior of these channels must be one from the following list.

* Zero outgoing Channels implies that this is the "end" of a Track and that Trap should dispose of the received marble.

* One outgoing Channel simply requires the Upstream Trap to pass on any marble that arrives out through that outgoing Channel.

* Multiple outgoing Channels will have one of two behaviors:

  * Exclusive: When a marble is received via the single incoming channel, a "Guard Condition" is evaluated,  using the `Channel.evaluate_guard()` method, for each outgoing channel that possesses one. The first outgoing channel whose guard condition evaluates to True wil be used as the outgoing channel for that marble. If no channel evaluates to true then the marble is sent out via the default channel. The default channel will be the only outgoing channel what has no Guard Condition.


  * Parallel: When a marble is received via the single incoming channel, a new set of marbles - one for each outgoing channel - must be created and each sent out via its matching channel.  This is the default behavior for multiple outgoing Channels.


## Marble Management

---
* The `Marble` interface replaces `ControlFlow` interface in `lazarus`.

---

As explained in the section discussing [Tracks](#diagram-and-tracks), a Track is responsible for managing the Marbles moving within its instance. However, the Trap (which part of a Diagram instance) has not direct access to any Track instances, instead the Trap must use the Marble (which _is__ part of a Track instance) as a proxy for its parent Track.

This means that a Marble must provide methods that can take care of the following Management responsibilities:

* Disposal of a Marble when it arrives at an Upstream Trap that has no outgoing channels. This is done by invoking the `Marble.dispose()` method.

* The creation of a set of Marbles to replace a single Marble when that single marble arrives at a "parallel" Upstream Trap. This is done by invoking the `Marble.diverge()` method.

* The disposal af a set of Marble to be replaced by a single Marble when the all members of the set of marbles have arrived at a Downstream Trap. This is done by invoking the `Marble.converge()` method for each arriving Marble and it will only return the new, converged, Marble when all the required Marbles have arrived.

The responsibility for the creation of the initial Marble when the `Track.start()` method can be handled directly by the Track as the Marble must be created before being passes to any Trap. Therefore the Marble does not need to provide a method for this responsibility.

