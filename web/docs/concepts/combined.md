# Combining Flow of Control and Data Handling

Now that the flow of control and data handling have been explained separately now is the time to combine them to create a complete, if basic, workflow module.


## The Diagram class

Apart from the Diagram class, there is no real overlay between the two aforementioned concepts. (Overriding the Trap.perform_action() method was the closest it got.) The Diagram class was used in both cases as it is the single point of departure when construction a workflow.

The Diagram class in the combined module is the same as that of the `control_flow` one as the merging of the data handling happens in the Trap, Track and Channel classes.


## The Trap class

The Trap class now takes over responsibility, from the Diagram, for the WrappedAction class. Thus the `Diagram.add_action` from `control_flow` becomes the `Trap.add_action`.


## The Track class

In the combined module the Track class take over the responsibility for the Tray class. This means that the `Track.start()` now initializes the data for the Matched Tray instance as well as getting the first marble rolling.


## The Channel class

Responsibilities for the Guard instance now fall to the Channel class. Thus, like the Trap class, the `Diagram.add_guard` from `control_flow` becomes the `Channel.add_guard`.

