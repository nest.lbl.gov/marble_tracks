# Defining a Workflow

Until now, demonstrating concepts has be done by explicitly constructing a Diagram from its elements. This is not ideal, as it is language specific and requires people crafting the workflow to know at least the basics of the implementation language of the workflow. It is far better to be able to define a workflow in a declarative language and have the implementation build the Diagram for a specification in that language. This document lays out the concepts on how that can be done.

After a review of languages that might be used as a definition language for workflow, it was decide to use [TOML (Tom's Obvious Minimal Language)](https://toml.io/en/). It is well supported across implementation languages, it is compact, and its structure is well matched to the structure of a workflow.

 
## TOML (Tom's Obvious Minimal Language)

 The syntax for TOML is straightforward and well [documented in its own website](https://toml.io/en/v1.0.0) so that will not be repeated here. It structure boils down to be a collection of Tables that contain key-value pairs. Those tables can be specified in a hierarchy but it is important to remember that any "node" in the tree can either contain sub-tables __or__ key-value pairs but __not__ both.
 
Definition a workflow is simply a case of filling various [Arrays of Tables](https://toml.io/en/v1.0.0#array-of-tables) with the definition of the components of a workflow and establishing the inter-component relationships by means of identities (`id`s).


## Structure of a Definition

### Diagram Name

The name of the root of the set of Table defining a workflow is considered to be the name of that workflow. For example the follow snippet defines an Action in the "exclusive_traps" workflow.

    [[exclusive_traps.external.action]]
    id = "add_one"
    operation = "add one"

This allows for multiple workflows to be defined in a single file.


### Conceptual Sections

As has already been covered, a workflow can be broken up into different conceptual sections.

* Flow of Control
* Data Handling
* External Code

Each of this has their own section in a diagram containing the components pertaining to that concept. For example the following snippet defines two Binding that can be used as part of a Picker and a Putter respectively.

    [[exclusive_traps.data.picker]]
    id = "pick_identity_1"
    source = "IDENTITY_1"
    target = "input"
    
    [[exclusive_traps.data.putter]]
    id = "put_identity_1"
    source = "output"
    target = "IDENTITY_1"

Meanwhile the follow snippet defines an Action Trap that uses both the Action and Binding shown in the previous snippets.

    [[exclusive_traps.control.action]]
    id = "north"
    action = "add_one"
    pickers = ["pick_identity_1"]
    putters = ["put_identity_1"]

The following shows the full structure of a definition file.

---
_Note:_ The order of element within a file is not significant, except all key-value pairs for a particular table must be together. 

---

    <name>                   # Parent
        external             # Parent
            action           # Array of Tables (optional)
                id           # Value (unique within this array)
                operation    # Value (id used in ExternalsFactory)
            guard            # Array of Tables (optional)
                id           # Value (unique within this array)
                condition    # Value (id used in ExternalsFactory)
            transformation   # Array of Tables (optional)
                id           # Value (unique within this array)
                conversion   # Value (id used in ExternalsFactory)
        data                 # Parent
            tray             # Array of Tables (optional)
                id           # Value (id of compartment, unique within this array)
            picker           # Array of Tables (optional, defines Bindings)
                id           # Value (unique within this array)
                source       # Value (reference to a tray element)
                target       # Value (input id to an action)
                transform    # Value (optional, reference to transformation element)
            putter           # Array of Tables (optional, defines Bindings)
                id           # Value (unique within this array)
                source       # Value (output id from an action)
                target       # Value (reference to a tray element)
                transform    # Value (optional, reference to a transformation element)
        control              # Parent
            start            # Array of Tables (only one element allowed)
                id           # Value (unique within this array)
            end              # Array of Tables (must be at least one)
                id           # Value (unique within this array)
            converging       # Array of Tables (optional)
                id           # Value (unique within this array)
                exclusive    # Value (optional, true if this is an exclusive trap)
            diverging        # Array of Tables (optional)
                id           # Value (unique within this array)
                exclusive    # Value (optional, true if this is an exclusive trap)
            action           # Array of Tables (optional)
                id           # Value (unique within this array)
                action       # Value (reference to an action element)
                pickers      # Array of values (optional, reference to picker elements)
                putters      # Array of values (optional, reference to putter elements)
            channel          # Array of Tables
                id         # Value (optional, unique within this array)
                upstream     # Value (reference to an any trap - excluding "end")
                downstream   # Value (reference to an any trap - excluding "start")
                guard        # Value (optional, reference to a guard element)

From this, it can be seen that the shortest valid workflow is as follows (though it does nothing useful!)

    [[start_end.control.start]]
    id = "start"
    
    [[start_end.control.end]]
    id = "end"
    
    [[start_end.control.channel]]
    upstream = "start"
    downstream = "end"
