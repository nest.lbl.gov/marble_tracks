#  Calling External Code.

This document lays out the concepts behind how calls are made from a workflow out to external implementations. The workflow itself simply controls what action is run with what data. It does not in any way define the action itself (See [below](#scriptable_externals) for the exception). That implementation must take place outside of the workflow and there the has to be a mechanism by which the implementations are invoked.

There are [three places in a workflow](tracks.md) that require external implementations:

* When a Trap needs to perform its action.
* When a Channel needs to evaluate its guard condition.
* When a data element needs to be transformed, either by a picker or a putter.


## The ExternalsFactory interface

When a workflow is being instantiated it needs to be provided with a ExternalsFactory instance that will do the do the mapping between a workflow Action, a Guard or a transformation and its external implementation. The `ExternalsFactory.get_action()` method, when given a key that is used in the workflow definition, should return an implementation of an action, i.e. a call that can take the data that results from the Picker `Binding` definitions and return data using the putter `Bindings`.

Similarly, the `ExternalsFactory.get_guard()` method should return implementations that can be used as guard conditions, i.e. a call that can take the data of a workflow and return a boolean, and the `ExternalsFactory.get_transformation()`

The precise behavior of an ExternalsFactory if not defined as it can depend on the deployment environment. For example in standaline deployments the implementations may be created at the time their `get_` method is invoked, but in deployments with a server environment the server may create the implementations itself an load them into the ExternalsFactory before the workflow is instantiated.


## Scriptable Externals

Externals do not have to be hard-coded routines, it is also possible to allow for the workflow to run scripts that are included as part of the workflow. In this circumstance, the executable element is a binding between the script and a deployment hardness that can run the provided script. The leads to the addition of the following methods to the ExternalsFactory.

* create\_scripted\_action
* create\_scripted\_guard
* create\_scripted\_transformation.

Each of the method that a matching Factory that can produce the necessary implementation that bind the script to a harness.
