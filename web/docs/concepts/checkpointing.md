# Checkpointing an Execution

The purpose of checkpointing a workflow is to be able to pick up its execution from where it was left off after it has been interrupted. While it beyond the scope a simple framework to be able to checkpoint Actions (the Action developer is welcome to include their own checkpointing) the state of  a workflow itself can be captured when appropriate.


## Where to Checkpoint

The most basic checkpointing would be recording the initial conditions of an execution and, when that checkpoint is use for recovery, restart the whole workflow. This approach would require that Actions be able to work out whether they have already been executed no matter how much the workflow had already executed. That, in turn would require Actions to be conscience of what future Actions might do to give false information about the start of the Workflow.An obvious example is an Action that deletes some piece of data and, in some workflow, a different Action re-created that data.


## Checkpointing When Arriving at a Trap

To avoid type of problem mentions above, and only require an Action to know if it was the last Action executed (and thus skip itself if necessary), it makes sense to checkpoint the workflow as a Marble arrives at the next Trap. This means that when that checkpoint is use for recovery a Marble simple needs to be handed to the `Trap.receive_marble()` method of the last Trap in which it was that contained, with two exception.

1. The `Trap.receive_marble()` method of the Start Trap should not be called.

2. Outbound Marbles from a Diverging Trap will not all arrive at the next Trap at the same time, so there will be a period of time when some of the new outbound Marbles will still be contained by the upstream Diverging Trap while the rest will be head by their downstream Trap (or beyond).


### Recovery of a Start Trap

As noted in the previous section, te `Trap.receive_marble()` method of the Start Trap should not be called. Instead the `Trap.send_marble()` methods is the one that should be invoked. Moreover  Threading add in another wrinkle.

The introduction of Thread means that Track can now be queued, but, as things start, its initial Marble is not created until actual execution begins. The first question to answer is "should waiting Tracks be dealt with by the recovery mechanism or should the workflow user restart them?". The answer to this "Yes, they should be recovered" as the user's bookkeeping will expect these Tracks to run and, moreover, they have no way to know which are running and which are waiting. This means that the `Trap.send_marble()` method should only be invoked if the Track contains the initial Marble, if it does not then the `Trap.start()` method should be called.


### Recovery of a Diverging Trap

The recovery of a Diverging Trap needs to take into account that only some of the outgoing Marbles will have been passed to their downstream Traps as a new checkpoint will be made for each arrival at a downstream Trap. To allow for this situation, the assignment of a new Marble to an outbound Channel must be treated like the arrival of a Marble at an Trap that is internal to the Diverging Trap as there is still a singlar flow of control at this point so the checkpoint can be made after all outgoing Marbles are assigned. Then, during recovery, those Marbles that have not arrived at their downstream Traps can be used to invoke the `Trap.send_diverging()` method.


### Terminating a Track


