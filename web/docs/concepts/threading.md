# Threading

As presented so far, each Track executes in its own thread. When a workflow contains no parallel diverging traps it is fine, however it can be limiting when parallel routes may occur but can not execute simultaneously. Moreover as each executing Track has its own thread, starting too many Tracks at the same time could overwhelm the host. Both of these concerns can be mitigated by having  a pool of threads in which Action Trap, along with the processing the control flow until the next Action Trap is encountered, can be executed.


## Thread Pooling

The idea of a thread pool is not new in computer science and many languages have implementation already available. Therefore in this document we will only introduce the concepts required to create a generic thread pool that satisfies the needs of a workflow. Those concepts will captured in the `WorkflowExecutor` class.


## Execution Units

The first step of enabling a workflow to exploit of thread pool is to break the execution, currently in a single thread, into smaller execution units. As already discussed above, the natural smaller unit is one in which the Action of an Action Trap is performed and then the control flow is processed until the next Action Trap is encountered. This leads to a combining of the `Trap.perform_action()` and `Trap.send_marble()` methods into a new `Trap.__perform_action()` method that encapsulates the responsibilities of the new execution unit. This method can then be submitted to run in the thread pool with a call to `WorkflowExecutor.submit_action()`.


## Track Termination

When using a single thread for a Track its termination is easy to detect. The completion of the `Trap.send_marble()` method of the Start Trap, either by returning or by throwning an exception, will indicate the Track has finished its work. This is not true when the work of a Track is split into smaller execution units. Now the termination of a Track needs to be:

* checked at the end on each execution unit;
* recorded in a thread safe manner when it happens; and
* checked at the start of each execution unit to see if any unit has terminated execution, in which case the this unit should not execute.

These requirements can be addressed by making the `Track.__terminated` member data an thread safe boolean and extending the new `Trap.__perform_action()` method to check the Track status before proceeding and update the Track status, if application, at the end.


## Avoiding Resource Starvation

Using a pool of threads avoids overwhelming the host by requesting too many threads. However, it is still possible that running too many Tracks simultaneously may still result in resource starvation. One example would be the case here more Tracks are being started than are completing. In this case there can be a pileup of later Actions waiting for earlier Actions, across all Tracks, to execute. Resources allocated to those pending Actions can build up leading to resource starvation.

One of the simplest ways to avoid the resource starvation described above, is to limit the number of executing Tracks based on the number of threads allocated to the pool. (Some previous implementation attempted to resolve this by prioritizing the Actions so later Action ran before earlier action. In practice this had a similar affect, i.e. limiting the number of actually executing workflows low, but was significantly more complex and so has been abandoned.) To do this a second queue is introduced that can hold pending workflow "starts" until it is deemed that sufficient availability in the thread pool. The mechanism is implemented using two new methods: `WorkflowExecutor.track_ready()` that is called when a track is ready to run; and `WorkflowExecutor.track_terminated()` that is called when a track terminates.
