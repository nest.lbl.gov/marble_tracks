# Data Handling

This document lays out the concepts behind how data is handle by `marble-tracks`. By "Data Handling", we mean the mechanics that dictate how a piece data can be access and modified when needed by part of the workflow. There are [two places in a workflow](tracks.md) that interact with data:

* When a Trap needs to perform its action.
* When a Channel needs to evaluate its guard condition.


## Tray Analogy

As with a the flow of control, there is a simple visualization of data handling. In this case TV dinner trays are used. These are thin metal trays that are stamped with separate compartments for each part of the meal. Similarly the data available to a workflow can be seen as one of these trays with a identified compartment for each data item that the workflow can access.

Again building from upon the [concepts in the "flow of control"](tracks.md#a-diagram-and-tracks) where 'The instructions for creating such track and be captured in a Diagram', the instructions for create a Tray can be captured in the same Diagram and a new tray is built for each execution so that two different executions can not interfere with each other.


## A Diagram and Trays

As noted above, 'the instructions for create a Tray can be captured in the same Diagram'. This means that a Tray instance can be created by the `Diagram.create_tray()` method, and the `Tray.load()` method can be used to write the data for a workflow into its Tray instance before the workflow begins. Similarly the `Tray.extract()` can be used to read the data after the workflow has terminated.


## Picking and Putting Data Between a Tray and an Action

Within a workflow an action is something that will effect a change. In most cases that action will need some input data on which to base the change and it may also produce output data that results from the change. (An action that have neither input nor output data are not considered here as they have no impact on the data handling.) The action itself may be used in other workflows or even outside workflows altogether, therefore the action need only know about its input and output data and it needs to know nothing about the structure of the Tray. (This is just a concrete way of saying "The coupling of an action to a workflow should be minimized"). In order to do this, before the action is performed the necessary data from the Tray needs to be picked and, if necessary, transformed into the appropriate inputs for the action by using the `Tray.pick()` method. Similarly, once the action has been performed, the `Tray.put()` method will do the opposite task, i.e. taking the outputs from the action, transforming them (if necessary) and updating the contents of the Tray.

In order to pick data from a tray and pass it to an action as an argument, each argument of the action will need a Binding that specifies from where in the Tray the data will taken, transform it - if necessary - into the data to be used as the argument and, finally, label it with the arguments name. The first and lat requirement are taken care of by the `Binding.source()` and `Binding.target()` methods, while any transformation is done by invoking the  `Binding.transform()` method.


### WrappedActions

Given that the data will always need to be picked and put around a invocation of a action, it makes sense to capture all of this in an a WrappedAction that does all of this in its `WrappedAction.perform()` method. The WrappedAction itself is part of the Diagram as it is code by each instance of the workflow, while the Tray is the instance specific data of a workflow.


## Data for Guard Conditions

All the previous discussion has be about providing workflow data to, and receiving data back from, an Action, which, in this realm, is a proxy for the Trap class in flow of control. This section discusses accessing data when evaluation a Guard Condition.

Unlike an Action which may be used in different workflows or outside workflows completely, a Guard Condition is an integral part of a workflow. This means that there is no need for a picker to prepare data for the Guard Condition evaluation (and the is no need for as the result of a Condition is a simple True/False). This means that the dictionary of objects in the Tray can be handed directly to the Condition and be used as input the making the guard decision.



