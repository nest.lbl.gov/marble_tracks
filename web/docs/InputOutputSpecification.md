# Notes on InputOutputSpecification

The InputOutputSpecification definition can be confusing, especially with respect to multi-instance Activities. The purpose of this note is to capture the basics behind a working example, so it can act as a template for other ones.


## SPADE Pre-Shipper Example

The `pre-shipper` Activity in the SPADE application is used here to explain how DataObjects are handled within simple Activities. Here is the relevant XML snippets, the first being the ItemDefinition elements used last the in the document as part of an Activity.

```
  <itemDefinition id="_ingest_ticket" isCollection="false" structureRef="gov.lbl.nest.spade.activities.IngestTicket"/>
  <itemDefinition id="_ingest_ticket_collection" isCollection="true" structureRef="gov.lbl.nest.spade.activities.IngestTicket"/>
```

```
      <ioSpecification id="InputOutputSpecification_7">
        <dataInput id="DataInput_1" itemSubjectRef="_ingest_ticket" name="input1"/>
        <dataOutput id="DataOutput_1" itemSubjectRef="_ingest_ticket_collection" isCollection="true" name="output1"/>
        <inputSet id="InputSet_7">
          <dataInputRefs>DataInput_1</dataInputRefs>
        </inputSet>
        <outputSet id="OutputSet_7" name="Output Set 7">
          <dataOutputRefs>DataOutput_1</dataOutputRefs>
        </outputSet>
      </ioSpecification>
      <dataInputAssociation id="DataInputAssociation_6">
        <sourceRef>DataObject_4</sourceRef>
        <targetRef>DataInput_1</targetRef>
      </dataInputAssociation>
      <dataOutputAssociation id="DataOutputAssociation_5">
        <sourceRef>DataOutput_1</sourceRef>
        <targetRef>DataObject_2</targetRef>
      </dataOutputAssociation>
 ```

### DataInput/DataOutput elements

The DataInput/DataOutput elements enumerate the DataObjects that will be available to the Activity.

* id: Used to identify this element so it can be referenced as a component of an InputSet/OutputSet.
* itemSubjectRef: A reference to the type of Data this object will provide.
* name: A name by which this object can be references within any transformation.
* isCollection: True if this object is a collection of the type of object declared in `itemSubjectRef`.


### InputSet/OutputSet elements

An InputSet/OutputSet element define collections of DataInput/DataOutput elements that are all required to complete the data required for that set.

* Data?putRefs: These use the `id` of the respective elements to define the contents of the set.


### DataInputAssociation/DataOutputAssociation elements

The DataInputAssociation/DataOutputAssociation elements are to bind DataObjects within enclosing scope to those that will be available to the Activity. This bind may optionally include a transformation of the data. The SourceRef and TargetRef element, depending on the direction of the association should be workflow scope DataObjects and DataObjects available to the Activity, or vice versa.


## SPADE Shipper Example

The `shipper` Activity in the SPADE application is used here to explain how DataObjects are handled within multi-instance Activities. Here is the relevant XML snippet. The extra MultiInstanceLoopCharacteristics element is what make this a multi-instance Activity and adds complications to interpreting the data handling.

```
      <ioSpecification id="InputOutputSpecification_8">
        <dataInput id="DataInput_2" itemSubjectRef="_ingest_ticket" name="input1"/>
        <dataInput id="DataInput_3" itemSubjectRef="_ingest_ticket_collection" isCollection="true" name="DataObject_2"/>
        <inputSet id="InputSet_8" name="Input Set 8">
          <dataInputRefs>DataInput_2</dataInputRefs>
          <dataInputRefs>DataInput_3</dataInputRefs>
        </inputSet>
        <outputSet id="OutputSet_8" name="Output Set 8"/>
      </ioSpecification>
      <dataInputAssociation id="DataInputAssociation_8">
        <sourceRef>DataObject_2</sourceRef>
        <targetRef>DataInput_3</targetRef>
      </dataInputAssociation>
      <dataInputAssociation id="DataInputAssociation_9">
        <sourceRef>DataInput_4</sourceRef>
        <targetRef>DataInput_2</targetRef>
      </dataInputAssociation>
      <multiInstanceLoopCharacteristics id="MultiInstanceLoopCharacteristics_1">
        <loopDataInputRef>DataInput_3</loopDataInputRef>
        <inputDataItem xsi:type="tDataInput" id="DataInput_4" itemSubjectRef="_ingest_ticket" name="input1"/>
      </multiInstanceLoopCharacteristics>
```

This this example there is a InputDataItem element that effectively declares an extra DataInput element with the id "DataInput\_4". The "extra" element is used to break up the "DataInput\_3" input collection into individual data elements, with those elements being the DataInput with the id "DataInput\_2". The DataInputAssociation element "DataInputAssociation_9" is the declaration of that unpacking of the collection.

The data handling here can be summed up as:

<block>
The workflow scoped DataObject\_2 is bound to the collection DataInput\_3 by DataInputAssociation\_8. This collection is, in turn, broken up into its constituent elements by MultiInstanceLoopCharacteristics\_1, and each element is bound to DataInput\_4. Now, for each instance of DataInput\_4, i.e. for each element in the collection, an Activity is launched and the DataInput\_4 data is bound to the activity scope as DataInput\_2 by means of DataInputAssociation\_9.
</block>

(Clear as mud!)

