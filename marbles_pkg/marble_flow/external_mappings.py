"""
Contains the construction and execution of a Workflow that is a
diverging and converging pair of Exclusive Traps.
"""

from typing import Any, Dict, Optional

import logging
import os


from marble_flow.concepts.externals import (
    Binding,
    Diagram,
    ExternalsFactory,
)

GUARD_NAMES = [
    "english",
    "integer",
]


ENGLISH_NUMBERS = ["zero", "one", "two", "three"]


def add_one(args):
    """
    Adds 1 to the argument named "input"
    """
    results = {}
    results["output"] = args["input"] + 1
    return results


def english_to_int(value: str) -> int:
    """
    Changes the English word into its integer.
    """
    return ENGLISH_NUMBERS.index(value)


def int_to_english(value: int) -> str:
    """
    Changes an integer into its English word.
    """
    return ENGLISH_NUMBERS[value]


def equals_two_integer(workflow_data: Dict[str, Any]) -> bool:
    """
    True if DATA_IDENTITIES[0] is the numeral 1.
    """
    return workflow_data[DATA_IDENTITIES[0]] == 2


def equals_two_english(workflow_data: Dict[str, Any]) -> bool:
    """
    True if DATA_IDENTITIES[1] is the English word "two"
    """
    return workflow_data[DATA_IDENTITIES[1]] == "two"


DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]
INITIAL_DATA = {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "two"}
EXPECTED = [
    {DATA_IDENTITIES[0]: 2, DATA_IDENTITIES[1]: "two"},
    {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "three"},
]
NORTH_BOUND = [True, False]


def create_diagram(externals_factory: ExternalsFactory, guard_name: str) -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    end = diagram.add_end()
    merging = diagram.add_downstream_trap()
    merging.set_exclusive_mode(True)
    diagram.connect(merging, end)
    north = diagram.add_action_trap()
    action = externals_factory.get_action("add_one")
    north_picker_bindings = [Binding(DATA_IDENTITIES[0], "input")]
    north_putter_bindings = [Binding("output", DATA_IDENTITIES[0])]
    north.add_action(action, north_picker_bindings, north_putter_bindings)
    diagram.connect(north, merging)
    south = diagram.add_action_trap()
    south_picker_bindings = [
        Binding(
            DATA_IDENTITIES[1],
            "input",
            externals_factory.get_transformation("english_to_int"),
        )
    ]
    south_putter_bindings = [
        Binding(
            "output",
            DATA_IDENTITIES[1],
            externals_factory.get_transformation("int_to_english"),
        )
    ]
    south.add_action(action, south_picker_bindings, south_putter_bindings)
    diagram.connect(south, merging)
    splitting = diagram.add_upstream_trap()
    splitting.set_exclusive_mode(True)
    channel = diagram.connect(splitting, north)
    channel.add_guard(externals_factory.get_guard(guard_name))
    diagram.connect(splitting, south)
    start = diagram.add_start()
    diagram.connect(start, splitting)
    return diagram


def execute(
    externals_factory: ExternalsFactory, guard_name: str, initial_data: Dict[str, Any]
) -> Optional[Dict[str, Any]]:
    """
    Runs this flow of control.
    """
    diagram = create_diagram(externals_factory, guard_name)
    track = diagram.create_track()
    track.start(initial_data)
    if track.is_successful():
        return track.extract()
    return None


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    externals_factory = ExternalsFactory()
    externals_factory.add_action("add_one", add_one)
    externals_factory.add_guard("english", equals_two_english)
    externals_factory.add_guard("integer", equals_two_integer)
    externals_factory.add_transformation("english_to_int", english_to_int)
    externals_factory.add_transformation("int_to_english", int_to_english)

    for example in range(0, 2):
        results = execute(externals_factory, GUARD_NAMES[example], INITIAL_DATA)
        if results is None:
            logging.info("Workflow is incomplete or failed")
            return 2
        for key in results.keys():
            if (
                EXPECTED[example][key] != results[key]
            ):  # pylint: disable=unsubscriptable-object
                logging.info("Workflow failed")
                return 1
    logging.info("Workflows succeeded")
    return 0


if __name__ == "__main__":
    main()
