"""
Contains the definition of a Trap class
"""

from typing import List, Optional

import logging

from .workflow_executor import WorkflowExecutor
from .binding import Binding
from .downstream import Downstream
from .channel import Channel
from .marble import Marble
from .upstream import Upstream
from .wrapped_action import Action, WrappedAction


class Trap(Downstream, Upstream):
    """
    This class represents the object that, upon the recept of a Marble,
    can perform some action.
    """

    def __init__(
        self,
        incoming_limit: Optional[int] = None,
        outgoing_limit: Optional[int] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__workflow_executor: Optional[WorkflowExecutor] = None
        self.__exclusive_mode: Optional[bool] = None
        self.__incoming: List[Channel] = []
        self.__incoming_limit = incoming_limit
        self.__outgoing: List[Channel] = []
        self.__outgoing_limit = outgoing_limit
        self.__wrapped_action: Optional[WrappedAction] = None

    def add_action(
        self,
        action: Action,
        picker_bindings: Optional[List[Binding]] = None,
        putter_bindings: Optional[List[Binding]] = None,
    ) -> WrappedAction:
        """
        Adds a WrappedAction to this object.
        """
        if self.__outgoing_limit is not None:
            raise ValueError("An action has already been added to this Trap.")
        self.__wrapped_action = WrappedAction(action, picker_bindings, putter_bindings)
        logging.debug("Created WrappedWaction %s", self.__wrapped_action)
        return self.__wrapped_action

    def add_executor(self, workflow_executor: "WorkflowExecutor") -> None:
        """
        Adds an incoming WorkflowExecutor to this object.
        """
        if self.__workflow_executor is not None:
            raise ValueError("An executor has already been added to this Trap.")
        self.__workflow_executor = workflow_executor

    def add_incoming_channel(self, channel: "Channel") -> None:
        """
        Adds an incoming Channel to this object.
        """
        if self.__incoming_limit is not None and self.__incoming_limit == len(
            self.__incoming
        ):
            raise ValueError(f'Too many incoming Channels added to this Trap, "{self}"')
        self.__incoming.append(channel)

    def add_outgoing_channel(self, channel: Channel) -> None:
        """
        Adds an outgoing channel to this Trap.
        """
        if self.__outgoing_limit is not None and self.__outgoing_limit == len(
            self.__outgoing
        ):
            raise ValueError("Too many outgoing Channels added to this Trap")
        self.__outgoing.append(channel)

    def incoming_size(self) -> int:
        """
        Returns the number of incoming Channels.
        """
        return len(self.__incoming)

    def outgoing_size(self) -> int:
        """
        Returns the number of outgoing Channels.
        """
        return len(self.__outgoing)

    def __perform_action(self, marble: Marble) -> None:
        """
        Executes this object's responsibilities by performing its action
        and sending the marble on.
        """
        if marble.is_terminated():
            return
        try:
            # Note that this check is here to satisfy mypy as
            #     this method is only called when this following
            #     is true.
            if self.__wrapped_action is not None:
                self.__wrapped_action.perform(marble.get_tray())
            self.send_marble(marble)
        except Exception as failure:  # pylint: disable=broad-exception-caught
            marble.set_failure(failure)

    def receive_marble(self, marble: Marble) -> None:
        """
        Receives the Marble from a Channel.
        """
        size = len(self.__incoming)
        if 0 == size:
            # This is an "start" Trap.
            return
        logging.debug("Marble being received at %s", self)
        if 1 == size:
            if self.__wrapped_action is None:
                self.send_marble(marble)
                return
            if self.__workflow_executor is None:
                self.__perform_action(marble)
                return
            logging.debug("Submitting %s for execution", self)
            self.__workflow_executor.submit_action(self.__perform_action, marble)
            return
        # Only get here when multiple channel Downstream Trap.

        # If exclusive or default mode
        if self.__exclusive_mode is None or self.__exclusive_mode is True:
            self.send_marble(marble)
            return

        # Otherwise is parallel
        convergence = marble.converge(self)
        if convergence is not None:
            self.send_marble(convergence)

    def send_marble(self, marble) -> None:
        """
        Send the Marble to one or more Channels.
        """
        size = self.outgoing_size()
        if 0 == size:
            # This is an "end" Trap.
            marble.dispose()
            return
        logging.debug("Marble being sent from %s", self)
        if 1 == size:
            self.__outgoing[0].transfer_marble(marble)
            return
        # Only get here when multiple channel Upstream Trap.

        # If parallel or default mode
        if self.__exclusive_mode is None or self.__exclusive_mode is not True:
            divergence = marble.diverge(self)
            index = 0
            for diverging in divergence:
                self.__outgoing[index].transfer_marble(diverging)
                index = index + 1
            return

        # Otherwise is exclusive
        default: Optional[Channel] = None
        for outgoing in self.__outgoing:
            guard = outgoing.evaluate_guard(marble)
            if guard is None:
                if default is not None:
                    raise ValueError("This Trap has more than one default Channel")
                default = outgoing
            elif guard is True:
                outgoing.transfer_marble(marble)
                return
        if default is None:
            raise ValueError("This Trap has more no default Channel")
        default.transfer_marble(marble)

    def set_exclusive_mode(self, exclusive_mode: bool) -> None:
        """
        Set the exclusive mode for this object.
        """
        if self.__exclusive_mode is not None:
            raise ValueError("The exclusive mode has already been set for this Trap.")
        self.__exclusive_mode = exclusive_mode
