"""
Contains the code that will display the structure of a Toml file.
"""

from typing import cast, Callable, Dict, Optional


import logging
import os
import sys

from tomlkit.container import Container
from tomlkit.items import AoT, Item, Table
from tomlkit.toml_file import TOMLFile

from .binding import Binding, Transformation
from .channel import Guard
from .diagram import Diagram
from .externals_factory import ExternalsFactory
from .trap import Trap
from .workflow_executor import WorkflowExecutor
from .wrapped_action import Action


def build_diagram(
    specfication: Item | Container | None,
    externals_factory: ExternalsFactory = ExternalsFactory(),
    workflow_executor: Optional[WorkflowExecutor] = None,
):
    """
    Builds a new Diagram instance based on the supplied specification.
    """
    diagram = Diagram(workflow_executor)

    if not isinstance(specfication, Table):
        raise ValueError("The specfication of a Diagram must be a Table")

    if "external" in specfication:
        external_element = specfication["external"]
    else:
        external_element = None
    actions = create_actions(external_element, externals_factory)
    guards = create_guards(external_element, externals_factory)
    transformations = create_transformations(external_element, externals_factory)

    if "data" in specfication:
        data_element = specfication["data"]
    else:
        data_element = None
    pickers = create_bindings(data_element, transformations, True)
    putters = create_bindings(data_element, transformations, False)

    if "control" in specfication:
        control_element = specfication["control"]
    else:
        control_element = None
    traps = create_traps(
        diagram,
        control_element,
        actions,
        pickers,
        putters,
        workflow_executor,
    )
    create_channels(control_element, diagram, traps, guards)

    return diagram


def configure_action_trap(  # pylint: disable=too-many-branches,too-many-arguments
    trap: Trap,
    spec: Table,
    actions: Dict[str, Action],
    pickers: Dict[str, Binding],
    putters: Dict[str, Binding],
    workflow_executor: Optional[WorkflowExecutor] = None,
) -> None:
    """
    Configures a new Action Trap.
    """
    action_identity = spec["action"]
    if action_identity is None:
        raise ValueError("Action must be specifed")
    if action_identity not in actions:
        raise ValueError(f"There is no action identified as {action_identity}")
    action = actions[str(action_identity)]  # cast only to stop "mypy" complaint
    picker_bindings = spec["pickers"]
    if picker_bindings is None:
        picker = None
    else:
        picker = []
        for (
            picker_identity
        ) in picker_bindings.unwrap():  # Need to unwrap Array to stop "mypy" complaint
            if picker_identity not in pickers:
                raise ValueError(f"There is no picker identified as {picker_identity}")
            picker.append(pickers[picker_identity])
    putter_bindings = spec["putters"]
    if putter_bindings is None:
        putter = None
    else:
        putter = []
        for (
            putter_identity
        ) in putter_bindings.unwrap():  # Need to unwrap Array to stop "mypy" complaint
            if putter_identity not in putters:
                raise ValueError(f"There is no putter identified as {putter_identity}")
            putter.append(putters[putter_identity])
    trap.add_action(
        action,
        picker,
        putter,
    )
    if workflow_executor is not None:
        trap.add_executor(workflow_executor)


def create_actions(
    external: Item | Container | None, externals_factory: ExternalsFactory
) -> Dict[str, Action]:
    """
    Creates an dictionary of Action operations from the supplied "external" specification.
    """
    actions: Dict[str, Action] = {}
    if external is None:
        return actions
    if not isinstance(external, Table):
        raise ValueError('The "external" element must be a Table')
    if "action" not in external:
        return actions
    specs = external["action"]
    if not isinstance(specs, AoT):
        raise ValueError('The "external.action" element must be an Array of Tables')
    for spec in specs:
        action_identity = spec["id"]
        if action_identity is None:
            raise ValueError("Action has no identity")
        operation = spec["operation"]
        if operation is None:
            raise ValueError("Operation must be specifed")
        result = externals_factory.get_action(operation)
        logging.debug("Created Action %s", result)
        actions[action_identity] = result
    return actions


def create_bindings(  # pylint: disable=too-many-branches
    data: Item | Container | None, transformations: Dict[str, Transformation], is_picker
) -> Dict[str, Binding]:
    """
    Creates an dictionary of Bindings from the supplied "data" specification.
    """
    bindings: Dict[str, Binding] = {}
    if data is None:
        return bindings
    if not isinstance(data, Table):
        raise ValueError('The "data" element must be a Table')
    if is_picker:
        specs = data["picker"]
        if not isinstance(specs, AoT):
            raise ValueError('The "external.picker" element must be an Array of Tables')
    else:
        specs = data["putter"]
        if not isinstance(specs, AoT):
            raise ValueError('The "external.putter" element must be an Array of Tables')
    if specs is not None:
        for spec in specs:
            if "id" not in spec:
                raise ValueError("Binding has no identity")
            binding_identity = spec["id"]
            if binding_identity is None:
                raise ValueError("Binding has no identity")
            source = spec["source"]
            if source is None:
                raise ValueError("Source must be specifed")
            target = spec["target"]
            if target is None:
                raise ValueError("Target must be specifed")
            result = Binding(source, target)
            if "transformation" in spec:
                transformation_identity = spec["transformation"]
                if transformation_identity is not None:
                    if transformation_identity not in transformations:
                        raise ValueError(
                            f"There is no transformation identified as {transformation_identity}"
                        )
                    result.add_transformation(transformations[transformation_identity])
            logging.debug("Created Binding %s", result)
            bindings[binding_identity] = result
    return bindings


def create_channels(  # pylint: disable=too-many-branches
    control: Item | Container | None,
    diagram: Diagram,
    traps: Dict[str, Trap],
    guards: Dict[str, Guard],
) -> None:
    """
    Creates the channels between Traps.
    """
    if control is None:
        return
    if not isinstance(control, Table):
        raise ValueError('The "control" element must be a Table')
    if "channel" not in control:
        return
    specs = control["channel"]
    if not isinstance(specs, AoT):
        raise ValueError('The "external.channel" element must be an Array of Tables')
    if specs is not None:
        for spec in specs:
            if "upstream" not in spec:
                raise ValueError("Upstream must be specifed")
            upstream_identity = spec["upstream"]
            if upstream_identity is None:
                raise ValueError("Upstream must be specifed")
            if upstream_identity not in traps:
                raise ValueError(f"There is no trap identified as {upstream_identity}")
            upstream = traps[upstream_identity]
            if "downstream" not in spec:
                raise ValueError("Downstream must be specifed")
            downstream_identity = spec["downstream"]
            if downstream_identity is None:
                raise ValueError("Downstream must be specifed")
            if downstream_identity not in traps:
                raise ValueError(
                    f"There is no trap identified as {downstream_identity}"
                )
            downstream = traps[downstream_identity]
            channel = diagram.connect(upstream, downstream)
            if "guard" in spec:
                guard_identity = spec["guard"]
                if guard_identity is not None:
                    if guard_identity not in guards:
                        raise ValueError(
                            f"There is no guard identified as {guard_identity}"
                        )
                    channel.add_guard(guards[guard_identity])


def create_guards(
    external: Item | Container | None, externals_factory: ExternalsFactory
) -> Dict[str, Guard]:
    """
    Creates an dictionary of Guard conditions from the supplied "external" specification.
    """
    guards: Dict[str, Guard] = {}
    if external is None:
        return guards
    if not isinstance(external, Table):
        raise ValueError('The "external" element must be a Table')
    if "guard" not in external:
        return guards
    specs = external["guard"]
    if not isinstance(specs, AoT):
        raise ValueError('The "external.guard" element must be an Array of Tables')
    if specs is not None:
        for spec in specs:
            if "id" not in spec:
                raise ValueError("Guard has no identity")
            guard_identity = spec["id"]
            if guard_identity is None:
                raise ValueError("Guard has no identity")
            condition = spec["condition"]
            if condition is None:
                raise ValueError("Condition must be specifed")
            result = externals_factory.get_guard(condition)
            logging.debug("Created Guard %s", result)
            guards[guard_identity] = result
    return guards


def create_transformations(
    external: Item | Container | None, externals_factory: ExternalsFactory
) -> Dict[str, Transformation]:
    """
    Creates an dictionary of Transformation conversions from the supplied "external" specification.
    """
    transformations: Dict[str, Guard] = {}
    if external is None:
        return transformations
    if not isinstance(external, Table):
        raise ValueError('The "control" element must be a Table')
    if "transformation" not in external:
        return transformations
    specs = external["transformation"]
    if not isinstance(specs, AoT):
        raise ValueError(
            'The "external.transformation" element must be an Array of Tables'
        )
    if specs is not None:
        for spec in specs:
            if "id" not in spec:
                raise ValueError("Transformation has no identity")
            transformation_identity = spec["id"]
            if transformation_identity is None:
                raise ValueError("Transformation has no identity")
            conversion = spec["conversion"]
            if conversion is None:
                raise ValueError("Conversion must be specifed")
            result = externals_factory.get_transformation(conversion)
            logging.debug("Created Transformation %s", result)
            transformations[transformation_identity] = result
    return transformations


def create_traps(  # pylint: disable=too-many-branches,too-many-arguments,too-many-locals
    diagram: Diagram,
    control: Item | Container | None,
    actions: Dict[str, Action],
    pickers: Dict[str, Binding],
    putters: Dict[str, Binding],
    workflow_executor: Optional[WorkflowExecutor] = None,
):
    """
    Creates an dictionary of Traps from the supplied "control" specification.
    """
    traps: Dict[str, Trap] = {}
    if control is None:
        return traps
    if not isinstance(control, Table):
        raise ValueError('The "control" element must be a Table')
    for trap_type, trap_creation in [
        ("start", diagram.add_start),
        ("end", diagram.add_end),
        ("diverging", diagram.add_upstream_trap),
        ("converging", diagram.add_downstream_trap),
        ("action", diagram.add_action_trap),
    ]:
        create_trap = cast(Callable[[], Trap], trap_creation)
        if trap_type in control:
            specs = control[trap_type]
            if not isinstance(specs, AoT):
                raise ValueError(
                    f'The "external.{trap_type}" element must be an Array of Tables'
                )
            for spec in specs:
                if "id" not in spec:
                    raise ValueError("Trap has no identity")
                trap_identity = spec["id"]
                if trap_identity is None:
                    raise ValueError("Trap has no identity")
                trap = create_trap()
                if "diverging" == trap_type:
                    mode = spec["mode"]
                    if mode is not None and mode == "exclusive":
                        trap.set_exclusive_mode(True)
                if "converging" == trap_type:
                    mode = spec["mode"]
                    if mode is not None and mode == "parallel":
                        trap.set_exclusive_mode(False)
                if "action" == trap_type:
                    configure_action_trap(
                        trap,
                        spec,
                        actions,
                        pickers,
                        putters,
                        workflow_executor,
                    )
                logging.debug("Created %s trap %s", trap_type.capitalize(), trap)
                traps[trap_identity] = trap
    return traps


def main() -> int:
    """
    Read Toml from file and write out a clean version of the Toml to a
    new file.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)

    input_file = TOMLFile(sys.argv[1])
    document = input_file.read()
    diagram = build_diagram(document["diagram"])
    if diagram is None:
        raise ValueError("Diagram not created")
    return 0


if __name__ == "__main__":
    main()
