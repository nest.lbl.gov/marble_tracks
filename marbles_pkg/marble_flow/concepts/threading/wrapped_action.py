"""
Contains the definition of a WrappedAction class
"""

from typing import Any, Callable, Dict, List, Optional

import logging

from .binding import Binding
from .tray import Tray

Action = Callable[[Dict[str, Any]], Dict[str, Any]]
ScriptableAction = Callable[[List[str], Dict[str, Any]], Dict[str, Any]]


class WrappedAction:  # pylint: disable=too-few-public-methods
    """
    This class wraps a Callable object with its data picker and putter.
    """

    def __init__(
        self,
        action: Action,
        picker_bindings: Optional[List[Binding]],
        putter_bindings: Optional[List[Binding]],
    ):
        """
        Creates an instance of this class.
        """
        self.__action = action
        if picker_bindings is None:
            self.__picker: List[Binding] = []
        else:
            self.__picker = picker_bindings
        if putter_bindings is None:
            self.__putter: List[Binding] = []
        else:
            self.__putter = putter_bindings

    def perform(self, tray: Tray) -> None:
        """
        Performs this object's action using the supplied Tray and
        putting the results o0f the action back into the Tray.
        """
        if self.__picker is None:
            arguments: Dict[str, Any] = {}
        else:
            arguments = tray.pick(self.__picker)
        logging.debug("Performing WrappedAction %s", self.__action)
        results = self.__action(arguments)
        logging.debug(
            "Results from WrappedAction %s are:\n    %s", self.__action, results
        )
        if self.__putter is not None:
            tray.put(self.__putter, results)
