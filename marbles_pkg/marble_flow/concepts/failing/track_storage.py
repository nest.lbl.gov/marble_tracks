"""
Contains the definition of the TracksStorage interface
"""

# mypy: check-untyped-defs

from typing import Any, Callable, Dict, List, Optional

import xml.etree.ElementTree as ET

from .downstream import Downstream
from .marble import Marble
from .termination_callback import TerminationCallback
from .trap import Trap
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor


class TrackStorage:
    """
    This class defines the interface by which Tracks are checkpointed.
    """

    def track_started(self, marble: Marble):
        """
        Add the supplied Marble to storage.
        """
        # May be overloaded, but not required.

    def action_completed(
        self,
        marble: Marble,
        current: Dict[str, Any],
        changes: List[str],
    ) -> None:
        """
        Updates the workflow data in storage when an action has completed.
        """
        # May be overloaded, but not required.

    def convergent_marbles(
        self,
        trap: Downstream,
        current: List[Marble],
        converging: List[Marble],
        result: Marble,
    ) -> None:
        """
        Stores the results on a parallel convergence.
        """
        # May be overloaded, but not required.

    def destroyed_marble(self, current: List[Marble], marble: Marble) -> None:
        """
        Removed the supplied Marble from storage.
        """
        # May be overloaded, but not required.

    def divergent_marbles(
        self,
        trap: Upstream,
        current: List[Marble],
        diverging: Marble,
        result: List[Marble],
    ) -> None:
        """
        Stores the results on a parallel divergence.
        """
        # May be overloaded, but not required.

    def lane_completed(
        self,
        marble: Marble,
        current: Dict[str, Any],
        changes: List[str],
        index: int,
    ) -> None:
        """
        Updates the workflow data in storage after a Lane has completed.
        """
        # May be overloaded, but not required.

    def marble_received(self, trap: Trap, marble: Marble) -> None:
        """
        Updated to Trap in which the supplied marble is contained.
        """
        # May be overloaded, but not required.

    def recover_track(  # pylint: disable=too-many-arguments
        self,
        track: ET.Element,
        start: Upstream,
        downstream_methods: Dict[str, Callable[[Marble], None]],
        termination_callback: Optional[TerminationCallback] = None,
        workflow_executor: Optional[WorkflowExecutor] = None,
    ) -> Optional["Track"]:  # type: ignore
        """
        Recovers a Track from its stored information.
        """
        raise ValueError("This method must of orderridden")

    def tray_loaded(self, contents: Optional[Dict[str, Any]]) -> None:
        """
        Initializes the workflow data in storage.
        """
        # May be overloaded, but not required.
