"""
Contains the definition of the XmlTrackStorage interface
"""

# mypy: check-untyped-defs

from typing import Any, Callable, Dict, List, Optional

import xml.etree.ElementTree as ET

from .downstream import Downstream
from .marble import Marble
from .termination_callback import TerminationCallback
from .track import Track
from .track_storage import TrackStorage
from .trap import Trap
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor

INDEXED_SUFFIX = "[]"
ARRAYED_SUFFIX = "<>"


def _marshall_value(value: object) -> ET.Element:
    """
    Takes supplied python object and turns it into an element
    """
    if value is None:
        return ET.Element("null")
    if isinstance(value, list):
        result = ET.Element("values")
        for item in value:
            item_element = _marshall_value(item)
            result.append(item_element)
        return result
    result = ET.Element("value")
    if isinstance(value, str):
        value_type = "java.lang.String"
        result.text = value
    elif isinstance(value, int):
        value_type = "java.lang.Integer"
        result.text = str(value)
    else:
        raise ValueError(f"Data type {type(value)} is not currently supported")
    result.set("type", value_type)
    return result


def _recover_value(container: ET.Element) -> object:
    """
    Recovers the value from the supplied container.
    """
    value_element = container.find("value")
    if value_element is not None:
        return _unmarshall_value(value_element)
    values_element = container.find("values")
    if values_element is None:
        return None
    return _unmarshall_values(values_element)


def _unmarshall_value(value: ET.Element) -> object:
    """
    Takes supplied element and turns it into a python object.
    """
    value_type = value.get("type")
    recovered_value = value.text
    if value_type is not None and recovered_value is not None:
        match value_type:
            case "java.lang.String":
                return recovered_value
            case "java.lang.Integer":
                return int(recovered_value)
            case _:
                raise ValueError(f"Data type {value_type} is not currently supported")
    else:
        # Effectively the default type is "java.lang.String"
        return recovered_value


def _unmarshall_values(values: ET.Element) -> object:
    """
    Takes supplied element and turns it into a python object.
    """
    result: List[Any] = []
    for child in values:
        if child.tag == "null":
            result.append(None)
        if child.tag == "value":
            result.append(_unmarshall_value(child))
        elif child.tag == "values":
            result.append(_unmarshall_values(child))
        else:
            pass
    return result


class XmlTrackStorage(TrackStorage):
    """
    This class manages the storing of a Track checkpoint as an XML element.
    """

    def __init__(
        self,
        parent: "XmlMarbleFlowStorage",  # type: ignore
        track: Optional[Track] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__marble_elements: Dict[Marble, ET.Element] = {}
        self.__parent = parent
        self.__pending_updates: Dict[Marble, List[ET.Element]] = {}
        self.__track = track
        if self.__track is not None:
            self.__track.set_storage(self)
        self.__track_element = ET.Element("track")

    def action_completed(
        self,
        marble: Marble,
        current: Dict[str, Any],
        changes: List[str],
    ) -> None:
        """
        Updates the workflow data in storage when an action has completed.
        """
        if changes is None or 0 == len(changes):
            return
        if marble in self.__pending_updates:
            raise ValueError("Should not get here")
        marble_changes: List[ET.Element] = []
        for key in changes:
            compartment_element = ET.Element("compartment")
            name_element = ET.SubElement(compartment_element, "name")
            name_element.text = key
            value = current[key]
            if value is not None:
                value_element = _marshall_value(value)
                compartment_element.append(value_element)
            marble_changes.append(compartment_element)
        self.__pending_updates[marble] = marble_changes
        return

    def convergent_marbles(
        self,
        trap: Downstream,
        current: List[Marble],
        converging: List[Marble],
        result: Marble,
    ) -> None:
        """
        Stores the results on a parallel convergence.
        """
        # May be overloaded, but not required.
        created_element = ET.Element("marble")
        index_element = ET.SubElement(created_element, "index")
        index_element.text = "-1"
        self.__marble_elements[result] = created_element
        destroyed_elements: List[ET.Element] = []
        for convergent in converging:
            destroyed_element = self.__marble_elements.pop(convergent)
            destroyed_elements.append(destroyed_element)
        marbles_element = self.__track_element.find("marbles")
        if marbles_element is None:
            raise ValueError("Should never get here")
        self.__parent.convergent_marbles(
            trap, marbles_element, destroyed_elements, created_element
        )

    def destroyed_marble(self, current: List[Marble], marble: Marble) -> None:
        """
        Removed the supplied Marble from storage.
        """
        # May be overloaded, but not required.

    def divergent_marbles(
        self,
        trap: Upstream,
        current: List[Marble],
        diverging: Marble,
        result: List[Marble],
    ) -> None:
        """
        Stores the results of a parallel divergence.
        """
        destroyed_element = self.__marble_elements.pop(diverging)
        created_elements: List[ET.Element] = []
        for resulting in result:
            created_element = ET.Element("marble")
            created_elements.append(created_element)
            index_element = ET.SubElement(created_element, "index")
            index_element.text = str(resulting.get_index())
            self.__marble_elements[resulting] = created_element
        marbles_element = self.__track_element.find("marbles")
        if marbles_element is None:
            raise ValueError("Should never get here")
        self.__parent.divergent_marbles(
            trap, marbles_element, destroyed_element, created_elements
        )

    def lane_completed(
        self,
        marble: Marble,
        current: Dict[str, Any],
        changes: List[str],
        index: int,
    ) -> None:
        """
        Updates the workflow data in storage after a Lane has completed.
        """
        if changes is None or 0 == len(changes):
            return
        marble_element = self.__marble_elements[marble]
        tray_element = self.__track_element.find("tray")
        if tray_element is None:
            raise ValueError("Should not get here")
        marble_changes: List[ET.Element] = []
        for key in changes:
            compartment_element = ET.Element("compartment")
            name_element = ET.SubElement(compartment_element, "name")
            name_element.text = key
            value = current[key]
            if value is not None:
                value_element = _marshall_value(value)
                compartment_element.append(value_element)
            marble_changes.append(compartment_element)
        self.__parent.lane_completed(
            marble_element, tray_element, marble_changes, index
        )

    def launching_lanes(self, marble: Marble) -> None:
        """
        Stores the details about the Lanes being executed within the
        Trap in which the supplied marble is contained.
        """
        marble_element = self.__marble_elements[marble]
        lanes = marble.get_lanes()
        if lanes is None or 0 == len(lanes):
            raise ValueError("Should not get here")
        lanes_element = ET.Element("lanes")
        for lane in lanes:
            lane_element = ET.SubElement(lanes_element, "lane")
            lane_element.text = str(lane)
        self.__parent.launching_lanes(marble_element, lanes_element)

    def marble_received(self, trap: Trap, marble: Marble) -> None:
        """
        Updated to Trap in which the supplied marble is contained.
        """
        marble_element = self.__marble_elements[marble]
        tray_element = self.__track_element.find("tray")
        if tray_element is None:
            raise ValueError("Should not get here")
        marble_changes = self.__pending_updates.pop(marble, None)
        self.__parent.marble_received(
            trap, marble_element, tray_element, marble_changes
        )

    def __recover_data(self, track: ET.Element) -> Optional[Dict[str, Any]]:
        """
        Recovers the workflow data associated with this object.
        """
        tray = track.find("tray")
        if tray is None:
            return None
        result: Dict[str, Any] = {}
        compartments = tray.findall("compartment")
        if compartments is not None and 0 != len(compartments):
            for compartment in compartments:
                name = compartment.find("name")
                if name is None or name.text is None:
                    raise ValueError("Compartment must be named")
                value = _recover_value(compartment)
                result[name.text] = value
        return result

    def __recover_marble(
        self,
        marble_element: ET.Element,
        downstream_methods: Dict[str, Callable[[Marble], None]],
    ) -> None:
        """
        Recovers a Marble from its stored information.
        """
        if self.__track is None:
            raise ValueError("Should not get here")
        trap = marble_element.find("trap")
        if trap is None:
            raise ValueError("Should not get here")
        trap_identity = trap.text
        if trap_identity is None:
            raise ValueError("Recovered Marble must have an associated Trap")

        index = marble_element.find("index")
        if index is None:
            recovered_index: Optional[int] = None
            lanes = marble_element.find("lanes")
            if lanes is None:
                method_key = trap_identity
            else:
                lane_elements = lanes.findall("lane")
                recovered_lanes: List[int] = []
                if lane_elements is not None and 0 != len(lane_elements):
                    for lane_element in lane_elements:
                        if lane_element.text is not None:
                            recovered_lanes.append(int(lane_element.text))
                method_key = trap_identity + ARRAYED_SUFFIX
        else:
            value = index.text
            if value is None:
                recovered_index = None
            else:
                recovered_index = int(value)
            method_key = trap_identity + INDEXED_SUFFIX
        marble = self.__track.recover_marble(
            downstream_methods[method_key], recovered_index, recovered_lanes
        )
        self.__marble_elements[marble] = marble_element

    def __recover_marbles(
        self,
        marbles: List[ET.Element],
        downstream_methods: Dict[str, Callable[[Marble], None]],
    ) -> None:
        """
        Recovers the collection of Marbles recovered from the supplied
        ET.Element.
        """
        for marble in marbles:
            self.__recover_marble(marble, downstream_methods)

    def recover_track(  # pylint: disable=too-many-arguments
        self,
        track: ET.Element,
        start: Upstream,
        downstream_methods: Dict[str, Callable[[Marble], None]],
        termination_callback: Optional[TerminationCallback] = None,
        workflow_executor: Optional[WorkflowExecutor] = None,
    ) -> Optional[Track]:
        """
        Recovers a Track from its stored information.
        """
        self.__track_element = track
        label = track.find("label")
        if label is None:
            label_to_use: Optional[str] = None
        else:
            label_to_use = label.text
        self.__track = Track(
            start,
            label_to_use,
            termination_callback,
            workflow_executor,
        )
        self.__track.set_storage(self)

        recovered_data = self.__recover_data(track)
        self.__track.load_recovered_data(recovered_data)

        marbles = track.findall("marbles/marble")
        if marbles is None or 0 == len(marbles):
            # The newly created Track is in the correct state for a
            # "waiting" Track.
            return self.__track

        self.__recover_marbles(marbles, downstream_methods)
        return self.__track

    def track_started(self, marble: Marble) -> None:
        """
        Add the supplied Marble to storage.
        """
        marbles_element = ET.Element("marbles")
        marble_element = ET.SubElement(marbles_element, "marble")
        self.__marble_elements[marble] = marble_element
        trap_element = ET.SubElement(marble_element, "trap")
        self.__parent.track_started(self.__track_element, marbles_element, trap_element)

    def tray_loaded(self, contents: Optional[Dict[str, Any]]) -> None:
        """
        Initializes the workflow data in storage.
        """
        if self.__track is None:
            raise ValueError("Track as not been assigned")
        label = self.__track.get_label()
        if label is not None:
            label_element = ET.SubElement(self.__track_element, "label")
            label_element.text = label
        tray_element = ET.SubElement(self.__track_element, "tray")
        if contents is not None and 0 != len(contents):
            for key in contents.keys():
                compartment_element = ET.SubElement(tray_element, "compartment")
                name_element = ET.SubElement(compartment_element, "name")
                name_element.text = key
                value = contents[key]
                if value is not None:
                    value_element = _marshall_value(value)
                    compartment_element.append(value_element)
        self.__parent.append_track(self.__track_element)
