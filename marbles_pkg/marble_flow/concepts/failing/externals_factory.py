"""
Contains the definition of a ExternalsFactory class
"""

# mypy: check-untyped-defs

from typing import Dict, List
from .binding import Transformation
from .channel import Guard
from .language_factory import LanguageFactory
from .wrapped_action import Action


class ExternalsFactory:
    """
    This class provide a workflow in external implementation that
    execute the responsibilities of its actions and guard conditions.
    """

    def __init__(self):
        """
        Creates an instance of this class.
        """
        self.__actions: Dict[str, Action] = {}
        self.__guards: Dict[str, Guard] = {}
        self.__language_factories: Dict[str, LanguageFactory] = {}
        self.__transformations: Dict[str, Transformation] = {}

    def add_action(self, key: str, action: Action) -> None:
        """
        Adds an action implementation to this object.
        """
        self.__actions[key] = action

    def add_scripted_action_factory(self, key: str, action: Action) -> None:
        """
        Adds an action implementation to this object.
        """
        self.__actions[key] = action

    def add_guard(self, key: str, guard: Guard) -> None:
        """
        Adds a guard implementation to this object.
        """
        self.__guards[key] = guard

    def add_language_factory(self, language: str, factory: LanguageFactory) -> None:
        """
        Adds a guard implementation to this object.
        """
        self.__language_factories[language] = factory

    def add_transformation(self, key: str, transformation: Transformation) -> None:
        """
        Adds an transformation implementation to this object.
        """
        self.__transformations[key] = transformation

    def create_scripted_action(self, language: str, script: List[str]) -> Action:
        """
        Creates a scriptable action implementation to this object.
        """
        return self.__language_factories[language].create_action(script)

    def create_scripted_guard(self, language: str, script: List[str]) -> Guard:
        """
        Creates a scriptable guard implementation to this object.
        """
        return self.__language_factories[language].create_guard(script)

    def create_scripted_transformation(
        self, language: str, script: List[str]
    ) -> Transformation:
        """
        Creates a scriptable transformation implementation to this object.
        """
        return self.__language_factories[language].create_transformation(script)

    def get_action(self, key: str) -> Action:
        """
        Returns the action implementation matching the supplied key.
        """
        return self.__actions[key]

    def get_guard(self, key: str) -> Guard:
        """
        Returns the guard implementation matching the supplied key.
        """
        return self.__guards[key]

    def get_transformation(self, key: str) -> Transformation:
        """
        Returns the transformation implementation matching the supplied
        key.
        """
        return self.__transformations[key]
