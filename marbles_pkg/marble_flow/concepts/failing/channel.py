"""
Contains the definition of a Channel class
"""

from typing import Any, Callable, Dict, Optional

import logging

from .marble import Marble
from .downstream import Downstream

Guard = Callable[[Dict[str, Any]], bool]


class Channel:
    """
    This class represents a link between two Trap classes.
    """

    def __init__(self, downstream: Downstream):
        self.__downstream = downstream
        self.__downstream.add_incoming_channel(self)
        self.__guard: Optional[Guard] = None

    def add_guard(self, guard: Guard) -> None:
        """
        Adds a Guard to this object.
        """
        if self.__guard is not None:
            raise ValueError("A Guard has already been added to this Channel.")
        logging.debug("Adding Guard %s", guard)
        self.__guard = guard

    def evaluate_guard(self, marble: Marble) -> Optional[bool]:
        """
        Returns True if the marble should be output via this object.
        """
        if self.__guard is None:
            return None
        workflow_data = marble.get_tray().extract()
        if workflow_data is None:
            return self.__guard({})
        return self.__guard(workflow_data)

    def transfer_marble(self, marble: Marble) -> None:
        """
        Transfers a Marble from the upstream trap to the downstream one.
        """
        self.__downstream.receive_marble(marble)
