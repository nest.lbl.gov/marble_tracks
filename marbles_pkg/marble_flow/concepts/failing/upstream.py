"""
Contains the definition of a Upstream class
"""

from .marble import Marble
from .channel import Channel


class Upstream:
    """
    This class represents the object that is at the upstream end of on
    or more channels.
    """

    def add_outgoing_channel(self, channel: Channel) -> None:
        """
        Adds an outgoing channel to this object.
        """

    def outgoing_size(self) -> int:
        """
        Returns the number of outgoing Channels.
        """
        return 0

    def send_marble(self, marble: Marble) -> None:
        """
        Send the Marble to one or more Channels.
        """
