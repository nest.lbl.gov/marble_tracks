"""
Contains the definition of a Track class
"""

# mypy: check-untyped-defs

from typing import Any, Callable, Dict, List, Optional

import logging
from threading import Event
import traceback

from .marble import Marble

from .downstream import Downstream
from .termination_callback import TerminationCallback
from .track_storage import TrackStorage
from .tray import Tray
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor


class Track:  # pylint: disable=too-many-instance-attributes
    """
    This class represents a concrete instance of the flow of control for
    a workflow defined in a Diagram.
    """

    def __init__(
        self,
        start: Upstream,
        label: Optional[str] = None,
        termination_callback: Optional[TerminationCallback] = None,
        workflow_executor: Optional[WorkflowExecutor] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__converging: Dict[Downstream, List[Marble]] = {}
        self.__failure: Optional[Exception] = None
        self.__label = label
        self.__marbles: List[Marble] = []
        self.__storage: Optional[TrackStorage] = None
        self.__terminated: Optional[Event] = None
        self.__termination_callback = termination_callback
        self.__tray = Tray()
        logging.debug("Created Tray %s", self.__tray)
        self.__upstream = start
        self.__workflow_executor = workflow_executor

    def abandon(self, marble: Marble) -> None:
        """
        Abandons this object so it is not longer in use.
        """
        return self.__destroy_marble(marble)

    def converge(self, trap: Downstream, marble: Marble) -> Optional[Marble]:
        """
        Adds this Marble to the set of Marbles arriving at the supplied
        Downstream object. If set of Marbles is complete an new Marble
        will be created and returned while disposing of the Marbles in
        the set. If the set is not complete, then None is returned.
        """
        if trap not in self.__converging:
            converging: List[Marble] = []
            self.__converging[trap] = converging
        else:
            converging = self.__converging[trap]
        if marble in converging:
            raise ValueError("This Marble has already be handled by this Trap")
        if len(converging) != (trap.incoming_size() - 1):
            # If wait for more arriving Marble, as this to list ans do nothing more.
            converging.append(marble)
            return None
        # All Marbles have arrived, so execute convergence.
        converging.append(marble)
        result = self.__create_marble()

        # clean up after convergence executed.
        for converged in converging:
            self.__destroy_marble(converged)
        if self.__storage is not None:
            self.__storage.convergent_marbles(trap, self.__marbles, converging, result)
        converging.clear()
        return result

    def __create_marble(self, index: Optional[int] = None) -> Marble:
        """
        Create a new Marble in this object
        """
        marble = Marble(self, index)
        self.__marbles.append(marble)
        logging.debug("Created Marble %s", marble)
        return marble

    def __destroy_marble(self, marble: Marble) -> None:
        """
        Removes the supplied Marble from this object.
        """
        try:
            self.__marbles.remove(marble)
            logging.debug("Destroyed Marble %s", marble)
        except ValueError:
            raise ValueError(  # pylint: disable=raise-missing-from
                "This Marble is not currently part of this Track"
            )
        if self.__terminated is None:
            raise ValueError("This Track has not been started")
        if 0 == len(self.__marbles):
            self.__set_terminated()

    def dispose(self, marble: Marble) -> None:
        """
        Disposes of the supplied Marble.
        """
        self.__destroy_marble(marble)
        if self.__storage is not None:
            self.__storage.destroyed_marble(self.__marbles, marble)

    def diverge(self, trap: Upstream, marble: Marble) -> List[Marble]:
        """
        Creates a set of Marbles that will replace this object passing
        out of the supplied Upstream object, while disposing of this
        object.
        """
        result: List[Marble] = []
        for index in range(trap.outgoing_size()):
            result.append(self.__create_marble(index))

        # clean up after convergence executed.
        self.__destroy_marble(marble)
        if self.__storage is not None:
            self.__storage.divergent_marbles(trap, self.__marbles, marble, result)
        return result

    def extract(self) -> Optional[Dict[str, Any]]:
        """
        Extracts the current workflow data items from this object.
        """
        return self.__tray.extract()

    def get_label(self) -> Optional[str]:
        """
        Returns the label, if any, for this object.
        """
        return self.__label

    def get_storage(self) -> Optional[TrackStorage]:
        """
        Returns the TrackStorage instance associated with this object.
        """
        return self.__storage

    def get_tray(self) -> Tray:
        """
        Returns the Tray holding the workflow data item for this object.
        """
        return self.__tray

    def is_failure(self, timeout: Optional[float] = None) -> Optional[Exception]:
        """
        Blocks until this object has terminated or times out, returning
        the Exception, if any, that cause it to fail. Lack of a failure
        does NOT indicate whether this object has terminated.
        """
        if self.__terminated is None:
            return None
        wait_completed = self.__terminated.wait(timeout)
        if wait_completed is True:
            return self.__failure
        return None

    def is_successful(self, timeout: Optional[float] = None) -> Optional[bool]:
        """
        Blocks until this object has terminated, returning True if it
        has terminated successfully, False if an Exception was raised
        and None if this object has not terminated yet.
        """
        if self.__terminated is None:
            return None
        wait_completed = self.__terminated.wait(timeout)
        if wait_completed is True:
            return self.__failure is None
        return None

    def is_terminated(self) -> Optional[bool]:
        """
        Returns True if this object has terminated,  either by completing or
        throwing an exception.
        """
        if self.__terminated is None:
            return None
        return self.__terminated.is_set()

    def is_terminating(self) -> Optional[bool]:
        """
        Returns True if this object is in the process of terminating.
        """
        if self.__terminated is None:
            return None
        return self.__failure is not None

    def load_recovered_data(
        self, recovered_data: Optional[Dict[str, Any]] = None
    ) -> None:
        """
        Load the Tray contain in this object with the supplied recovered_data.
        """
        self.__tray.load(recovered_data)
        if recovered_data is not None and 0 != len(recovered_data):
            logging.debug("Loaded recovered data into Tray %s", self.__tray)

    def __marbles_restart(self) -> None:
        """
        Restarts all the Marbles contained in this object.
        """
        # create a copy so there is not failure if a marble is deleted
        # for the list in this object during the loop.
        marbles_to_use = self.__marbles[:]
        for marble in marbles_to_use:
            marble.restart()

    def recover_marble(
        self,
        restart_method: Callable[[Marble], None],
        index: Optional[int] = None,
        lanes: Optional[List[int]] = None,
    ) -> Marble:
        """
        Recover a stored Marble belonging to this object
        """
        if self.__terminated is None:
            self.__terminated = Event()
        result = Marble(self, index, restart_method, lanes)
        self.__marbles.append(result)
        logging.debug("Recovered Marble %s", result)
        return result

    def restart(self) -> None:
        """
        Restarts this object after it has been recovered.
        """
        if self.__terminated is None:
            # If means that it is waiting and has not started executing.
            self.__schedule_execution(self.__run)
            return
        self.__schedule_execution(self.__marbles_restart)

    def __run(self) -> None:
        """
        Moves the Marbles this object manages through the Diagram.
        """
        self.__terminated = Event()
        failed_marble: Optional[Marble] = None
        try:
            marble = self.__create_marble()
            failed_marble = marble
            if self.__storage is not None:
                self.__storage.track_started(marble)
            self.__upstream.send_marble(marble)
        except Exception as failure:  # pylint: disable=broad-exception-caught
            self.set_failure(failed_marble, failure)

    def __schedule_execution(self, run_method: Callable[[], None]):
        """
        Schedules the execution of this object.
        """
        if self.__workflow_executor is None:
            run_method()
        else:
            self.__workflow_executor.track_ready(self, run_method)

    def set_failure(self, marble: Optional[Marble], failure: Exception) -> None:
        """
        Set the Exception that caused this object to fail.
        """
        if self.__terminated is None:
            raise ValueError("This Track has not been started")
        stack = traceback.format_exc()
        if self.__failure is None:
            self.__failure = failure
            logging.error("Failure has happened")
        else:
            logging.error("Additional Failure has happened")
        if marble is not None:
            self.__destroy_marble(marble)
        else:
            self.__set_terminated()
        logging.error(stack)

    def set_recovered_marbles(self, marbles: List[Marble]) -> None:
        """
        Set the supplied Marbles as the Marbles in this object.
        """
        self.__marbles = marbles

    def set_storage(self, storage: TrackStorage) -> None:
        """
        Sets the TrackStorage instance to be used by this class.
        """
        if self.__storage is not None:
            raise ValueError("TrackStorage has already been added to this object")
        self.__storage = storage

    def __set_terminated(self):
        """
        Sets this object as terminated.
        """
        if self.__terminated is None:
            raise ValueError("This Track has not been started")
        self.__terminated.set()
        if self.__workflow_executor is None:
            logging.debug("Track terminated")
        else:
            self.__workflow_executor.track_terminated(self)
        if self.__termination_callback is not None:
            self.__termination_callback.terminated(self, self.__failure)

    def start(self, initial_data: Optional[Dict[str, Any]] = None) -> None:
        """
        Create an initial marble and starts it going though this object.
        """
        self.__tray.load(initial_data)
        if self.__storage is not None:
            self.__storage.tray_loaded(self.__tray.extract())
        if initial_data is not None and 0 != len(initial_data):
            logging.debug("Loaded initial data, if any, into Tray %s", self.__tray)
        self.__schedule_execution(self.__run)
