"""
Contains the definition of the XmlTrackStorage interface
"""

# mypy: check-untyped-defs

from typing import Any, Callable, Dict, List, Optional

import xml.etree.ElementTree as ET

from .downstream import Downstream
from .marble import Marble
from .termination_callback import TerminationCallback
from .track import Track
from .track_storage import TrackStorage
from .trap import Trap
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor


class XmlTrackStorage(TrackStorage):
    """
    This class manages the storing of a Track checkpoint as an XML element.
    """

    def __init__(
        self,
        parent: "XmlMarbleFlowStorage",  # type: ignore
        track: Optional[Track] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__marble_elements: Dict[Marble, ET.Element] = {}
        self.__parent = parent
        self.__pending_updates: Dict[Marble, List[ET.Element]] = {}
        self.__track = track
        if self.__track is not None:
            self.__track.set_storage(self)
        self.__track_element = ET.Element("track")

    def convergent_marbles(
        self,
        trap: Downstream,
        current: List[Marble],
        converging: List[Marble],
        result: Marble,
    ):
        """
        Stores the results on a parallel convergence.
        """
        # May be overloaded, but not required.
        created_element = ET.Element("marble")
        index_element = ET.SubElement(created_element, "index")
        index_element.text = "-1"
        self.__marble_elements[result] = created_element
        destroyed_elements: List[ET.Element] = []
        for convergent in converging:
            destroyed_element = self.__marble_elements.pop(convergent)
            destroyed_elements.append(destroyed_element)
        marbles_element = self.__track_element.find("marbles")
        if marbles_element is None:
            raise ValueError("Should never get here")
        self.__parent.convergent_marbles(
            trap, marbles_element, destroyed_elements, created_element
        )

    def destroyed_marble(self, current: List[Marble], marble: Marble):
        """
        Removed the supplied Marble from storage.
        """
        # May be overloaded, but not required.

    def divergent_marbles(
        self,
        trap: Upstream,
        current: List[Marble],
        diverging: Marble,
        result: List[Marble],
    ):
        """
        Stores the results of a parallel divergence.
        """
        destroyed_element = self.__marble_elements.pop(diverging)
        created_elements: List[ET.Element] = []
        for resulting in result:
            created_element = ET.Element("marble")
            created_elements.append(created_element)
            index_element = ET.SubElement(created_element, "index")
            index_element.text = str(resulting.get_index())
            self.__marble_elements[resulting] = created_element
        marbles_element = self.__track_element.find("marbles")
        if marbles_element is None:
            raise ValueError("Should never get here")
        self.__parent.divergent_marbles(
            trap, marbles_element, destroyed_element, created_elements
        )

    def marble_received(self, trap: Trap, marble: Marble):
        """
        Updated to Trap in which the supplied marble is contained.
        """
        tray_element = self.__track_element.find("tray")
        if tray_element is None:
            raise ValueError("Should not get here")
        marble_changes = self.__pending_updates.pop(marble, None)
        marble_element = self.__marble_elements[marble]
        self.__parent.marble_received(
            trap, marble_element, tray_element, marble_changes
        )

    def __marshall_value(self, value: object) -> ET.Element:
        """
        Takes supplied python object and turns it into an element
        """
        if isinstance(value, str):
            value_type = "java.lang.String"
        elif isinstance(value, int):
            value_type = "java.lang.Integer"
        else:
            raise ValueError(f"Data type {type(value)} is not currently supported")
        result = ET.Element("value")
        result.text = str(value)
        result.set("type", value_type)
        return result

    def __recover_data(self, track: ET.Element) -> Optional[Dict[str, Any]]:
        """
        Recovers the workflow data associated with this object.
        """
        tray = track.find("tray")
        if tray is None:
            return None
        result: Dict[str, Any] = {}
        compartments = tray.findall("compartment")
        if compartments is not None and 0 != len(compartments):
            for compartment in compartments:
                name = compartment.find("name")
                if name is None or name.text is None:
                    raise ValueError("Compartment must be named")
                value = self.__recover_value(compartment)
                result[name.text] = value
        return result

    def __recover_marble(
        self,
        marble_element: ET.Element,
        downstream_methods: Dict[str, Callable[[Marble], None]],
    ) -> None:
        """
        Recovers a Marble from its stored information.
        """
        if self.__track is None:
            raise ValueError("Should not get here")
        trap = marble_element.find("trap")
        if trap is None:
            raise ValueError("Should not get here")
        trap_identity = trap.text
        if trap_identity is None:
            raise ValueError("Recovered Marble must have an associated Trap")

        index = marble_element.find("index")
        if index is None:
            recovered_index: Optional[int] = None
        else:
            value = index.text
            if value is None:
                recovered_index = None
            else:
                recovered_index = int(value)
        if index is None:
            method_key = trap_identity
        else:
            method_key = trap_identity + "[]"
        marble = self.__track.recover_marble(
            downstream_methods[method_key], recovered_index
        )
        self.__marble_elements[marble] = marble_element

    def __recover_marbles(
        self,
        marbles: List[ET.Element],
        downstream_methods: Dict[str, Callable[[Marble], None]],
    ) -> None:
        """
        Recovers the collection of Marbles recovered from the supplied
        ET.Element.
        """
        for marble in marbles:
            self.__recover_marble(marble, downstream_methods)

    def recover_track(  # pylint: disable=too-many-arguments
        self,
        track: ET.Element,
        start: Upstream,
        downstream_methods: Dict[str, Callable[[Marble], None]],
        termination_callback: Optional[TerminationCallback] = None,
        workflow_executor: Optional[WorkflowExecutor] = None,
    ) -> Optional[Track]:
        """
        Recovers a Track from its stored information.
        """
        self.__track_element = track
        label = track.find("label")
        if label is None:
            label_to_use: Optional[str] = None
        else:
            label_to_use = label.text
        self.__track = Track(
            start,
            label_to_use,
            termination_callback,
            workflow_executor,
        )
        self.__track.set_storage(self)

        recovered_data = self.__recover_data(track)
        self.__track.load_recovered_data(recovered_data)

        marbles = track.findall("marbles/marble")
        if marbles is None or 0 == len(marbles):
            # The newly created Track is in the correct state for a
            # "waiting" Track.
            return self.__track

        self.__recover_marbles(marbles, downstream_methods)
        return self.__track

    def __recover_value(self, container: ET.Element) -> object:
        """
        Recovers the value from the supplied container.
        """
        value = container.find("value")
        if value is not None:
            return self.__unmarshall_value(value)
        values = container.find("values")
        if values is None:
            return None
        collection = values.findall("value")
        result: List[Any] = []
        if collection is None or 0 == len(collection):
            return result
        for value in collection:
            recovered_value = self.__unmarshall_value(value)
            result.append(recovered_value)
        return result

    def track_started(self, marble: Marble) -> None:
        """
        Add the supplied Marble to storage.
        """
        marbles_element = ET.Element("marbles")
        marble_element = ET.SubElement(marbles_element, "marble")
        self.__marble_elements[marble] = marble_element
        trap_element = ET.SubElement(marble_element, "trap")
        self.__parent.track_started(self.__track_element, marbles_element, trap_element)

    def tray_loaded(self, contents: Optional[Dict[str, Any]]) -> None:
        """
        Initializes the workflow data in storage.
        """
        if self.__track is None:
            raise ValueError("Track as not been assigned")
        label = self.__track.get_label()
        if label is not None:
            label_element = ET.SubElement(self.__track_element, "label")
            label_element.text = label
        tray_element = ET.SubElement(self.__track_element, "tray")
        if contents is not None and 0 != len(contents):
            for key in contents.keys():
                compartment_element = ET.SubElement(tray_element, "compartment")
                name_element = ET.SubElement(compartment_element, "name")
                name_element.text = key
                value = contents[key]
                if value is not None:
                    value_element = self.__marshall_value(value)
                    compartment_element.append(value_element)
        self.__parent.append_track(self.__track_element)

    def tray_updated(
        self,
        marble: Marble,
        current: Dict[str, Any],
        changes: List[str],
        index: Optional[int] = None,
    ):
        """
        Updates the workflow data in storage.
        """
        if changes is None or 0 == len(changes):
            return
        if marble in self.__pending_updates:
            raise ValueError("Should not get here")
        marble_changes: List[ET.Element] = []
        for key in changes:
            compartment_element = ET.Element("compartment")
            name_element = ET.SubElement(compartment_element, "name")
            name_element.text = key
            value = current[key]
            if value is not None:
                value_element = self.__marshall_value(value)
                compartment_element.append(value_element)
            marble_changes.append(compartment_element)
        self.__pending_updates[marble] = marble_changes

    def __unmarshall_value(self, value: ET.Element) -> object:
        """
        Takes supplied element and turns it into a python object.
        """
        value_type = value.get("type")
        recovered_value = value.text
        if value_type is not None and recovered_value is not None:
            match value_type:
                case "java.lang.String":
                    return recovered_value
                case "java.lang.Integer":
                    return int(recovered_value)
                case _:
                    raise ValueError(
                        f"Data type {value_type} is not currently supported"
                    )
        else:
            # Effectively the default type is "java.lang.String"
            return recovered_value
