"""
Contains the definition of the XmlMarbleFlowStorage class
"""

# mypy: check-untyped-defs

from typing import Callable, Dict, List, Optional

from pathlib import Path
import xml.etree.ElementTree as ET

from .marble import Marble
from .marbleflow_storage import MarbleFlowStorage
from .termination_callback import TerminationCallback
from .track import Track
from .track_storage import TrackStorage
from .trap import Trap
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor
from .xml_track_storage import XmlTrackStorage


class XmlMarbleFlowStorage(
    MarbleFlowStorage
):  # pylint: disable=too-many-instance-attributes
    """
    This class manages the storing for workflow checkpoints in a single XML file.
    """

    def __init__(
        self,
        store_location: Path,
        workflow_executor: Optional[WorkflowExecutor] = None,
        termination_callback: Optional[TerminationCallback] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__counter: Optional[int] = 1
        self.__downstream_methods: Dict[str, Callable[[Marble], None]] = {}
        self.__start_identity: str = ""
        self.__store_location = store_location
        self.__termination_callback = termination_callback
        self.__trap_identities: Dict[Trap, str] = {}
        self.__upstream: Optional[Upstream] = None
        self.__workflow_executor = workflow_executor
        if self.__store_location.exists():
            tree = ET.parse(self.__store_location)
            self.__marbleflow_element = tree.getroot()
            return
        parent = self.__store_location.parent
        if not parent.exists():
            parent.mkdir()
        self.__marbleflow_element = ET.fromstring(
            """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<marble_flow>
</marble_flow>
"""
        )
        self.save()

    def add_track(self, track: Track) -> TrackStorage:
        """
        Puts a new track into the storage.
        """
        result = XmlTrackStorage(self, track)
        return result

    def add_start(
        self, trap: Upstream, downstream_method: Callable[[Marble], None], identity: str
    ) -> None:
        """
        Adds Start Trap details to this object.
        """
        if self.__upstream is not None:
            raise ValueError("Start Trap has already been set")
        self.__start_identity = identity
        self.__downstream_methods[identity] = downstream_method
        self.__upstream = trap

    def add_trap(
        self,
        trap: Trap,
        downstream_method: Callable[[Marble], None],
        identity: str,
        indexed: bool = False,
    ) -> None:
        """
        Adds Trap (other than Start) details to this object.
        """
        if indexed:
            self.__downstream_methods[identity + "[]"] = downstream_method
        else:
            self.__downstream_methods[identity] = downstream_method
        self.__trap_identities[trap] = identity

    def append_track(self, track: ET.Element):
        """
        Appends a Track that has just started to the current XML document.
        """
        tracks = self.__marbleflow_element.find("tracks")
        if tracks is None:
            tracks = ET.SubElement(self.__marbleflow_element, "tracks")
        tracks.append(track)
        self.save()

    def convergent_marbles(
        self,
        trap: Trap,
        marbles_element: ET.Element,
        destroyed_elements: List[ET.Element],
        created_element: ET.Element,
    ):
        """
        Stores the results of a parallel convergence.
        """
        for destroyed_element in destroyed_elements:
            marbles_element.remove(destroyed_element)
        marbles_element.append(created_element)
        trap_element = ET.SubElement(created_element, "trap")
        trap_element.text = self.__trap_identities[trap]
        self.save()

    def divergent_marbles(
        self,
        trap: Trap,
        marbles_element: ET.Element,
        destroyed_element: ET.Element,
        created_elements: List[ET.Element],
    ):
        """
        Stores the results of a parallel divergence.
        """
        marbles_element.remove(destroyed_element)
        for created_element in created_elements:
            marbles_element.append(created_element)
            trap_element = ET.SubElement(created_element, "trap")
            trap_element.text = self.__trap_identities[trap]
        self.save()

    def marble_received(
        self,
        trap: Trap,
        marble_element: ET.Element,
        tray_element: ET.Element,
        marble_changes: List[ET.Element],
    ):
        """
        Updated to Trap in which the supplied marble is contained.
        """
        index_element = marble_element.find("index")
        if index_element is not None:
            marble_element.remove(index_element)
        trap_element = marble_element.find("trap")
        if trap_element is None:
            raise ValueError("Should never get here")
        trap_element.text = self.__trap_identities[trap]

        # Update Tray with the result from the previous trap that
        # contained this marble.
        if marble_changes is not None and 0 != len(marble_changes):
            for marble_change in marble_changes:
                name_element = marble_change.find("name")
                if name_element is None or name_element.text is None:
                    raise ValueError("Should never get here")
                name = name_element.text
                value_element = marble_change.find("value")
                existing_element = tray_element.find(f"compartment/[name = '{name}']")
                if existing_element is None:
                    raise ValueError(f"Compartment {name} does not exist")
                existing_value_element = existing_element.find("value")
                if existing_value_element is not None:
                    existing_element.remove(existing_value_element)
                if value_element is not None:
                    existing_element.append(value_element)
        self.save()

    def recover_tracks(
        self,
    ) -> List[Track]:
        """
        Returns the collection of track recovered from the
        store_location of this object.
        """
        if self.__upstream is None:
            raise ValueError("The Start Trap has not been set for recovery")

        tracks = self.__marbleflow_element.findall("tracks/track")
        result: List[Track] = []
        if tracks is not None:
            for track in tracks:
                track_storage = XmlTrackStorage(self)
                recovered_track = track_storage.recover_track(
                    track,
                    self.__upstream,
                    self.__downstream_methods,
                    self.__termination_callback,
                    self.__workflow_executor,
                )
                if recovered_track is not None:
                    result.append(recovered_track)
        return result

    def save(self):
        """
        Save the current XML document to the storage location
        """
        tree = ET.ElementTree(self.__marbleflow_element)
        ET.indent(tree, "    ")
        if self.__counter is None:
            location_to_use = self.__store_location
        else:
            location_to_use = self.__store_location.parent.joinpath(
                str(self.__store_location.stem)
                + f".{self.__counter}"
                + str(self.__store_location.suffix)
            )
            self.__counter += 1
        tree.write(location_to_use, encoding="UTF-8", xml_declaration=True)

    def track_started(
        self,
        track_element: ET.Element,
        marbles_element: ET.Element,
        trap_element: ET.Element,
    ):
        """
        Sets the supplied Trap to be the Start Trap identity
        """
        trap_element.text = self.__start_identity
        track_element.append(marbles_element)
        self.save()
