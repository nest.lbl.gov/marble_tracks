"""
Contains the definition of the StorageBuilder interface
"""


from typing import Optional


from .termination_callback import TerminationCallback
from .workflow_executor import WorkflowExecutor


class StorageBuilder:  # pylint: disable=too-few-public-methods
    """
    This class manages the creation of new StorageManager instances.
    """

    def create_manager(
        self,
        workflow_executor: Optional[WorkflowExecutor] = None,
        termination_callback: Optional[TerminationCallback] = None,
    ):
        """
        Creates a new StorageManager instance.
        """
        raise ValueError("This method must be overloaded")
