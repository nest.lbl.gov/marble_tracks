"""
Contains the definition of a Diagram class
"""

# mypy: check-untyped-defs

from typing import Any, Dict, List, Optional

from .downstream import Downstream
from .channel import Channel
from .storage_builder import StorageBuilder
from .termination_callback import TerminationCallback
from .trap import Trap
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor
from .track import Track


def get_spec_value(spec: Dict[str, Any], label: str) -> Optional[str]:
    """
    Extracts a value from the supplied specification, returning None if
    the requested item deas not exist.
    """
    if "name" in spec:
        return spec[label]
    return None


class Diagram:
    """
    This class contain the design from which Track objects can be created.
    """

    def __init__(
        self,
        workflow_executor: Optional[WorkflowExecutor] = None,
        termination_callback: Optional[TerminationCallback] = None,
        storage_builder: Optional[StorageBuilder] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__start: Optional[Upstream] = None
        if storage_builder is not None:
            self.__storage = storage_builder.create_manager(
                workflow_executor, termination_callback
            )
        self.__termination_callback = termination_callback
        self.__workflow_executor = workflow_executor

    def add_action_trap(self, identity: str) -> Trap:
        """
        Adds an intermediate intermediate Trap to this object.
        """
        result = Trap(incoming_limit=1)
        if self.__storage is not None:
            self.__storage.add_trap(result, result.receive_marble, identity)
        return result

    def add_downstream_trap(self, identity: str) -> Trap:
        """
        Adds an intermediate intermediate Trap to this object.  This is
        used to create a converging Trap.
        """
        result = Trap(outgoing_limit=1)
        if self.__storage is not None:
            self.__storage.add_trap(result, result.receive_marble, identity)
            self.__storage.add_trap(result, result.send_converged, identity, True)
        return result

    def add_end(self, identity: str) -> Downstream:
        """
        Adds an end Trap to this object.
        """
        result = Trap(
            incoming_limit=1,
            outgoing_limit=0,
        )
        result.set_exclusive_mode(True)
        if self.__storage is not None:
            self.__storage.add_trap(result, result.receive_marble, identity)
        return result

    def add_start(self, identity: str) -> Upstream:
        """
        Adds a start Trap to this object.
        """
        if self.__start is not None:
            raise ValueError()
        self.__start = Trap(
            incoming_limit=0,
            outgoing_limit=1,
        )
        self.__start.set_exclusive_mode(False)
        if self.__storage is not None:
            self.__storage.add_start(self.__start, self.__start.send_marble, identity)
        return self.__start

    def add_upstream_trap(self, identity: str) -> Trap:
        """
        Adds an intermediate intermediate Trap to this object. This is
        used to create a diverging Trap.
        """
        result = Trap(incoming_limit=1)
        if self.__storage is not None:
            self.__storage.add_trap(result, result.receive_marble, identity)
            self.__storage.add_trap(result, result.send_diverging, identity, True)
        return result

    def connect(self, upstream: Upstream, downstream: Downstream) -> Channel:
        """
        Connects the specified Traps to each other by mean of a Channel.
        """
        result = Channel(downstream)
        upstream.add_outgoing_channel(result)
        return result

    def create_track(self, label: Optional[str] = None) -> Track:
        """
        Create a new Track instance.
        """
        if self.__start is None:
            raise UnboundLocalError()
        result = Track(
            self.__start, label, self.__termination_callback, self.__workflow_executor
        )
        if self.__storage is not None:
            self.__storage.add_track(result)
        return result

    def recover_tracks(self) -> List[Track]:
        """
        Returns the collection of track recovered from the supplied store_location.
        """
        if self.__storage is None:
            raise ValueError("No MarbleFlowStorage has been assigned to this Diagram")
        return self.__storage.recover_tracks()
