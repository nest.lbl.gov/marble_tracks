"""
Contains the definition of the StorageBuilder interface
"""

from typing import Optional

from pathlib import Path

from .storage_builder import StorageBuilder
from .termination_callback import TerminationCallback
from .workflow_executor import WorkflowExecutor
from .xml_marbleflow_storage import XmlMarbleFlowStorage


class XmlStorageBuilder(StorageBuilder):  # pylint: disable=too-few-public-methods
    """
    This class manages the creation of XmlMarbleFlowStorage instances.
    """

    def __init__(self, storage_location: Path):
        """
        creates and instance of this class.
        """
        self.__storage_location = storage_location

    def create_manager(
        self,
        workflow_executor: Optional[WorkflowExecutor] = None,
        termination_callback: Optional[TerminationCallback] = None,
    ):
        """
        Creates a new MarbleFlowStorage instance.
        """
        return XmlMarbleFlowStorage(
            self.__storage_location, workflow_executor, termination_callback
        )
