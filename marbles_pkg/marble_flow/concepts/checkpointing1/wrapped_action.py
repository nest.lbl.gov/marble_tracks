"""
Contains the definition of a WrappedAction class
"""

from typing import Any, Callable, Dict, List, Optional

import logging

from .binding import Binding
from .lanes import Lanes
from .marble import Marble
from .tray import Tray

Action = Callable[[Dict[str, Any]], Dict[str, Any]]
ScriptableAction = Callable[[List[str], Dict[str, Any]], Dict[str, Any]]


class WrappedAction:
    """
    This class wraps a Callable object with its data picker and putter.
    """

    def __init__(
        self,
        action: Action,
        picker_bindings: Optional[List[Binding]] = None,
        putter_bindings: Optional[List[Binding]] = None,
        lanes: Optional[Lanes] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__action = action
        self.__lanes = lanes
        if picker_bindings is None:
            self.__pickers: List[Binding] = []
        else:
            self.__pickers = picker_bindings
        if putter_bindings is None:
            self.__putters: List[Binding] = []
        else:
            self.__putters = putter_bindings

    def get_lane_count(self, tray: Tray) -> int:
        """
        Returns the number of lanes that should be created by an Array Trap.
        """
        # -1 means that this is not an Array Trap.
        if self.__lanes is None:
            return -1
        return self.__lanes.get_count(tray)

    def perform(self, tray: Tray, marble: Optional[Marble] = None) -> None:
        """
        Performs this object's action using the supplied Tray and
        putting the results of the action back into the Tray.
        """
        if self.__pickers is None:
            arguments: Dict[str, Any] = {}
        else:
            arguments = tray.pick(self.__pickers)
        logging.debug("Performing WrappedAction %s", self.__action)
        results = self.__action(arguments)
        logging.debug(
            "Results from WrappedAction %s are:\n    %s", self.__action, results
        )
        if self.__putters is not None:
            with tray.get_lock():
                changes = tray.put(self.__putters, results)
                if marble is not None and marble.get_storage() is not None:
                    marble.get_storage().tray_updated(marble, tray, changes)

    def perform_lane(self, tray: Tray, marble: Marble, index: int) -> None:
        """
        Performs this object's action within a lane adding a Lane scope
        to the supplied supplied Tray and putting the results in that
        scope back into the Tray.
        """
        if self.__lanes is None:
            raise ValueError("Should never reach here")
        lane_tray = Tray(tray)
        lane_tray.load(self.__lanes.pick_lane_data(index, tray))
        self.perform(lane_tray)
        changes = self.__lanes.put_lane_data(index, lane_tray)
        if marble.get_storage() is not None:
            marble.get_storage().tray_updated(marble, tray, changes, index)
