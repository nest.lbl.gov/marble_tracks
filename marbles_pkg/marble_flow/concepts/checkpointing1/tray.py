"""
Contains the definition of a Tray class
"""

# mypy: check-untyped-defs

from typing import Any, Dict, List, Optional

from threading import Lock

from .binding import Binding

EMPTY_LIST_STR: List[str] = []


class Tray:
    """
    This class represents a concrete instance of the data structure
    used by a workflow defined in a Diagram.
    """

    def __init__(self, parent: Optional["Tray"] = None):  # type: ignore
        """
        Creates an instance of this class.
        """
        self.__contents: Optional[Dict[str, Any]] = None
        self.__lock = Lock()
        self.__parent = parent

    def extract(self) -> Optional[Dict[str, Any]]:
        """
        Extracts the current data from this object.
        """
        if self.__parent is None:
            outer_extract: Optional[Dict[str, Any]] = None
        else:
            outer_extract = self.__parent.extract()
        if self.__contents is not None:
            if outer_extract is None:
                # Currently this is a shallow copy!
                return self.__contents.copy()
            for key in self.__contents.keys():
                outer_extract[key] = self.__contents[key]
        return outer_extract

    def __getitem__(self, item: str) -> Any:
        if self.__contents is None:
            if self.__parent is None:
                raise KeyError(item)
            return self.__parent[item]
        if item in self.__contents:
            return self.__contents[item]
        if self.__parent is None:
            raise KeyError(item)
        return self.__parent[item]

    def get_lock(self) -> Lock:
        """
        Returns the Lock instance used to lock this object.
        """
        return self.__lock

    def load(self, initial_data: Optional[Dict[str, Any]] = None) -> None:
        """
        Loads this object with its initial data.
        """
        if self.__contents is not None:
            raise ValueError("This tray has already been loaded")
        if initial_data is None:
            self.__contents = {}
        else:
            # Currently this is a shallow copy!
            self.__contents = initial_data.copy()

    def keys(self) -> List[str]:
        """
        Returns all of he keys held in this object.
        """
        if self.__contents is None:
            if self.__parent is None:
                return EMPTY_LIST_STR
            return self.__parent.keys()
        if self.__parent is None:
            return list(self.keys())
        result = self.__parent.keys()
        result.extend(self.keys())
        return result

    def pick(self, bindings: List[Binding]) -> Dict[str, Any]:
        """
        Picks from and, if necessary transforms, the data in this object so it
        can be passed to an action.
        """
        result: Dict[str, Any] = {}
        for binding in bindings:
            source = self[binding.get_source()]
            result[binding.get_target()] = binding.transform(source)
        return result

    def put(
        self,
        bindings: List[Binding],
        results: Optional[Dict[str, Any]] = None,
    ) -> List[str]:
        """
        Puts and, if necessary transform, the data resulting from an
        action into the data in this object.
        """
        changed: List[str] = []
        if results is None:
            return changed
        if bindings is None:
            return changed
        for binding in bindings:
            source = results[binding.get_source()]
            target = binding.get_target()
            self.__update(target, binding.transform(source))
            changed.append(target)
        return changed

    def __update(self, target, value) -> None:
        if self.__contents is None or target not in self.__contents:
            if self.__parent is None:
                raise ValueError(f"This tray has no target {target}")
            self.__parent.__update(target, value)  # pylint: disable=protected-access
        else:
            self.__contents[target] = value
