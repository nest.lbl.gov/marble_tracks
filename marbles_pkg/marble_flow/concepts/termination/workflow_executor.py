"""
Contains the definition of the WorkflowExecutor class
"""

from typing import Callable, List, Tuple

import logging

from concurrent.futures import Executor, Future
from threading import Lock


class WorkflowExecutor:  # pylint: disable=too-few-public-methods
    """
    A thread pool in which Action Traps can execute their responsibilities.
    """

    def __init__(self, executor: Executor, track_limit: int = 0):
        """
        Creates an instance of this class.
        """
        self.__executor = executor
        self.__executing_tracks: List["Track"] = []  # type: ignore
        self.__lock = Lock()
        self.__pending_tracks: List[Tuple["Track", Callable[[], None]]] = []  # type: ignore
        self.__track_limit = track_limit

    def __run_track(
        self, track: "Track", run_method: Callable[[], None]  # type: ignore
    ) -> None:
        self.__executing_tracks.append(track)
        count = len(self.__executing_tracks)
        if 1 == count:
            plural = ""
        else:
            plural = "s"
        logging.debug("Track starting, so %i Track%s will be running", count, plural)
        run_method()

    def submit_action(self, action_execution, *args, **kwargs) -> Future:
        """
        Submits the specified action routine to be run with the supplied arguments.
        """
        return self.__executor.submit(action_execution, *args, **kwargs)

    def track_ready(
        self, track: "Track", run_method: Callable[[], None]  # type: ignore
    ) -> None:
        """
        Lets this object know that the specified Track is ready to be
        started when the resources are available.
        """
        with self.__lock:
            count = len(self.__executing_tracks)
            if self.__track_limit == 0 or self.__track_limit > count:
                self.__run_track(track, run_method)
            else:
                self.__pending_tracks.append((track, run_method))

    def track_terminated(self, track: "Track") -> None:  # type: ignore
        """
        Lets this object know that the specified Track has terminated
        and thus released its resources.
        """
        with self.__lock:
            self.__executing_tracks.remove(track)
            count = len(self.__executing_tracks)
            if 1 == count:
                plural = " is"
            else:
                plural = "s are"
            logging.debug("Track terminated, so %i Track%s now running", count, plural)
            if (
                self.__track_limit == 0
                or self.__track_limit > count
                and 0 != len(self.__pending_tracks)
            ):
                next_track, run_method = self.__pending_tracks.pop(0)
                self.__run_track(next_track, run_method)
