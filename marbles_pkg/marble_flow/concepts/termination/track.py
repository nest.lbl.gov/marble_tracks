"""
Contains the definition of a Track class
"""

from typing import Any, Dict, List, Optional

import logging
from threading import Event
import traceback

from .marble import Marble

from .downstream import Downstream
from .termination_callback import TerminationCallback
from .tray import Tray
from .upstream import Upstream
from .workflow_executor import WorkflowExecutor


class Track:  # pylint: disable=too-many-instance-attributes
    """
    This class represents a concrete instance of the flow of control for
    a workflow defined in a Diagram.
    """

    def __init__(
        self,
        start: Upstream,
        label: Optional[str] = None,
        termination_callback: Optional[TerminationCallback] = None,
        workflow_executor: Optional[WorkflowExecutor] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__convergencies: Dict[Downstream, List[Marble]] = {}
        self.__failure: Optional[Exception] = None
        self.__label = label
        self.__marbles: List[Marble] = []
        self.__terminated: Optional[Event] = None
        self.__termination_callback = termination_callback
        self.__tray = Tray()
        logging.debug("Created Tray %s", self.__tray)
        self.__upstream = start
        self.__workflow_executor = workflow_executor

    def converge(self, trap: Downstream, marble: Marble) -> Optional[Marble]:
        """
        Adds this Marble to the set of Marbles arriving at the supplied
        Downstream object. If set of Marbles is complete an new Marble
        will be created and returned while disposing of the Marbles in
        the set. If the set is not complete, then None is returned.
        """
        current = self.__convergencies[trap]
        if current is None:
            current = []
            self.__convergencies[trap] = current
        if marble in current:
            raise ValueError("This Marble has already be handled by this Trap")
        if len(current) != (trap.incoming_size() - 1):
            # If wait for more arriving Marble, as this to list ans do nothing more.
            current.append(marble)
            return None
        # All Marbles have arrived, so execute convergence.
        result = self.__create_marble()
        for converging in current:
            self.__destroy_marble(converging)
        current.clear()
        self.__destroy_marble(marble)
        return result

    def __create_marble(self) -> Marble:
        """
        Create as new Marble in this object
        """
        marble = Marble(self)
        self.__marbles.append(marble)
        logging.debug("Created Marble %s", marble)
        return marble

    def __destroy_marble(self, marble: Marble) -> None:
        """
        Removes the supplied Marble from this object
        """
        try:
            self.__marbles.remove(marble)
            logging.debug("Destroyed Marble %s", marble)
        except ValueError:
            raise ValueError(  # pylint: disable=raise-missing-from
                "This Marble is not currently part of this Track"
            )
        if self.__terminated is None:
            raise ValueError("This Track has not been started")
        if 0 == len(self.__marbles):
            self.__set_terminated()

    def dispose(self, marble: Marble) -> None:
        """
        Disposes of the supplied Marble.
        """
        self.__destroy_marble(marble)

    def diverge(self, trap: Upstream, marble: Marble) -> List[Marble]:
        """
        Creates a set of Marbles that will replace this object passing
        out of the supplied Upstream object, while disposing of this
        object.
        """
        result: List[Marble] = []
        for _ in range(trap.outgoing_size()):
            result.append(self.__create_marble())
        self.__destroy_marble(marble)
        return result

    def extract(self) -> Optional[Dict[str, Any]]:
        """
        Extracts the current workflow data items from this object.
        """
        return self.__tray.extract()

    def get_label(self) -> Optional[str]:
        """
        Returns the label, if any, for this object.
        """
        return self.__label

    def get_tray(self) -> Tray:
        """
        Returns the Tray holding the workflow data item for this object.
        """
        return self.__tray

    def is_failure(self, timeout: Optional[float] = None) -> Optional[Exception]:
        """
        Blocks until this object has terminated or times out, returning
        the Exception, if any, that cause it to fail. Lack of a failure
        does NOT indicate whether this object has terminated.
        """
        if self.__terminated is None:
            return None
        wait_completed = self.__terminated.wait(timeout)
        if wait_completed is True:
            return self.__failure
        return None

    def __run(self) -> None:
        """
        Moves the Marbles this object manages through the Diagram.
        """
        self.__terminated = Event()
        try:
            marble = self.__create_marble()
            self.__upstream.send_marble(marble)
        except Exception as failure:  # pylint: disable=broad-exception-caught
            self.set_failure(failure)

    def start(self, initial_data: Optional[Dict[str, Any]] = None) -> None:
        """
        Create an initial marble and starts it going though this object.
        """
        self.__tray.load(initial_data)
        if self.__workflow_executor is None:
            self.__run()
        else:
            self.__workflow_executor.track_ready(self, self.__run)

    def is_successful(self, timeout: Optional[float] = None) -> Optional[bool]:
        """
        Blocks until this object has terminated, returning True if it
        has terminated successfully, False if an Exception was raised
        and None if this object has not terminated yet.
        """
        if self.__terminated is None:
            return None
        wait_completed = self.__terminated.wait(timeout)
        if wait_completed is True:
            return self.__failure is None
        return None

    def is_terminated(self) -> Optional[bool]:
        """
        Returns True if this object has terminated,  either by completing or
        throwing an exception.
        """
        if self.__terminated is None:
            return None
        return self.__terminated.is_set()

    def set_failure(self, failure: Exception) -> None:
        """
        Set the Exception that caused this object to fail.
        """
        if self.__terminated is None:
            raise ValueError("This Track has not been started")
        stack = traceback.format_exc()
        self.__failure = failure
        self.__set_terminated()
        logging.error(stack)

    def __set_terminated(self):
        """
        Sets this object as terminated.
        """
        self.__terminated.set()
        if self.__workflow_executor is None:
            logging.debug("Track terminated")
        else:
            self.__workflow_executor.track_terminated(self)
        if self.__termination_callback is not None:
            self.__termination_callback.terminated(self, self.__failure)
