"""
Contains the definition of a Tray class
"""

# mypy: check-untyped-defs

from typing import Any, Dict, KeysView, List, Optional

from .binding import Binding

EMPTY_DICT: Dict[str, Any] = {}


class Tray:
    """
    This class represents a concrete instance of the data structure
    used by a workflow defined in a Diagram.
    """

    def __init__(
        self,
    ):
        """
        Creates an instance of this class.
        """
        self.__contents: Optional[Dict[str, Any]] = None

    def extract(self) -> Optional[Dict[str, Any]]:
        """
        Extracts the current data from this object.
        """
        if self.__contents is None:
            return None
        # Currently this is a shallow copy!
        return self.__contents.copy()

    def __getitem__(self, item: str) -> Any:
        if self.__contents is None:
            raise KeyError(item)
        return self.__contents[item]

    def load(self, initial_data: Optional[Dict[str, Any]] = None) -> None:
        """
        Loads this object with its initial data.
        """
        if self.__contents is not None:
            raise ValueError("This tray has already been loaded")
        if initial_data is None:
            self.__contents = {}
        else:
            # Currently this is a shallow copy!
            self.__contents = initial_data.copy()

    def keys(self) -> KeysView[str]:
        """
        Returns all of he keys held in this object.
        """
        if self.__contents is None:
            return EMPTY_DICT.keys()
        return self.__contents.keys()

    def pick(self, bindings: List[Binding]) -> Dict[str, Any]:
        """
        Picks from and, if necessary transforms, the data in this object so it
        can be passed to an action.
        """
        result: Dict[str, Any] = {}
        if self.__contents is None:
            raise ValueError("This tray has no sources")
        for binding in bindings:
            source = self.__contents[binding.get_source()]
            result[binding.get_target()] = binding.transform(source)
        return result

    def put(
        self,
        bindings: List[Binding],
        results: Optional[Dict[str, Any]] = None,
    ) -> None:
        """
        Puts and, if necessary transforms, the data resulting from an
        action into the data in this object.
        """
        if results is None:
            return
        if bindings is None:
            return
        if self.__contents is None:
            raise ValueError("This tray has no targets")
        for binding in bindings:
            source = results[binding.get_source()]
            self.__contents[binding.get_target()] = binding.transform(source)
