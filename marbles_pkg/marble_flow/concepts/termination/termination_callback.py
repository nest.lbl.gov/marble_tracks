"""
Contains the definition of a TerminationCallback class
"""

from typing import Optional


class TerminationCallback:  # pylint: disable=too-few-public-methods
    """
    This class is used as a callback when a Track terminates.
    """

    def terminated(self, track: "Track", failure: Optional[Exception]) -> None:  # type: ignore
        """
        Called when a Track terminates.
        """
