"""
Contains the definition of a Binding class
"""

from typing import Any, Callable, Optional

Transformation = Callable[[Any], Any]


class Binding:
    """
    This class represents a binding between two Data Objects.
    """

    def __init__(self, source: str, target: str):
        """
        Creates an instance of this class.
        """
        self.__source = source
        self.__target = target
        self.__transformation: Optional[Transformation] = None

    def add_transformation(self, transformation: Transformation) -> None:
        """
        Adds a Transformation to this object.
        """
        if self.__transformation is not None:
            raise ValueError("A transformation has already been added to this Binding.")
        self.__transformation = transformation

    def get_source(self):
        """
        Returns the identity of the object that is the source of the data.
        """
        return self.__source

    def get_target(self):
        """
        Returns the identity of the object that is the target of the data.
        """
        return self.__target

    def transform(self, source_data: Any):
        """
        Executes the transformation, in any, that should be applied to
        the source data in order to prepare it from being send to the
        target.
        """
        if self.__transformation is None:
            return source_data
        return self.__transformation(source_data)
