"""
Contains the definition of a Diagram class
"""

# mypy: check-untyped-defs

from typing import Any, Dict, Optional

from .downstream import Downstream
from .channel import Channel
from .upstream import Upstream
from .track import Track
from .trap import Trap


def get_spec_value(spec: Dict[str, Any], label: str) -> Optional[str]:
    """
    Extracts a value from the supplied specification, returning None if
    the requested item deas not exist.
    """
    if "name" in spec:
        return spec[label]
    return None


class Diagram:
    """
    This class contain the design from which Track objects can be created.
    """

    def __init__(self):
        """
        Creates an instance of this class.
        """
        self.__start: Optional[Upstream] = None

    def add_action_trap(self) -> Trap:
        """
        Adds an intermediate intermediate Trap to this object.
        """
        result = Trap(incoming_limit=1)
        return result

    def add_downstream_trap(self) -> Trap:
        """
        Adds an intermediate intermediate Trap to this object.
        """
        result = Trap(outgoing_limit=1)
        return result

    def add_end(self) -> Downstream:
        """
        Adds an end Trap to this object.
        """
        result = Trap(
            incoming_limit=1,
            outgoing_limit=0,
        )
        result.set_exclusive_mode(True)
        return result

    def add_start(self) -> Upstream:
        """
        Adds a start Trap to this object.
        """
        if self.__start is not None:
            raise ValueError()
        self.__start = Trap(
            incoming_limit=0,
            outgoing_limit=1,
        )
        self.__start.set_exclusive_mode(False)
        return self.__start

    def add_upstream_trap(self) -> Trap:
        """
        Adds an intermediate intermediate Trap to this object.
        """
        result = Trap(incoming_limit=1)
        return result

    def connect(self, upstream: Upstream, downstream: Downstream) -> Channel:
        """
        Connects the specified Traps to each other by mean of a Channel.
        """
        result = Channel(downstream)
        upstream.add_outgoing_channel(result)
        return result

    def create_track(self) -> Track:
        """
        Create a new Track instance.
        """
        if self.__start is None:
            raise UnboundLocalError()
        return Track(self.__start)
