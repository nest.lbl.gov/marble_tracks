"""
Contains the definition of a Lanes class
"""

from typing import Any, Dict, List, Optional


from .tray import Tray


class Distributor:
    """
    Contains the details of how to scatter data when starting Lanes.
    """

    def __init__(self, source: str, target: str, size: Optional[int] = None):
        """
        Creates an instance of this class.
        """
        self.__size = size
        self.__source = source
        self.__target = target

    def get_count(self, tray: Tray) -> int:
        """
        Returns the number of lanes that should be created by an Array Trap.
        """
        if self.__size is None:
            return len(tray[self.__source])
        return self.__size

    def pick_lane_data(self, index: int, tray: Tray) -> Dict[str, Any]:
        """
        Returns the data to be loaded into the Lane scope
        """
        if self.__size is None:
            return {self.__target: tray[self.__source][index], "words": None}
        return {self.__target: index, "words": None}


class Collector:  # pylint: disable=too-few-public-methods
    """
    Contains the details of how to gather data when Lanes have completed.
    """

    def __init__(self, source: str, target: str):
        """
        Creates an instance of this class.
        """
        self.__source = source
        self.__target = target

    def put_lane_data(self, index: int, tray: Tray) -> List[str]:
        """
        Returns the data to be loaded into the Lane scope
        """
        tray[self.__target][index] = tray[self.__source]
        changed: List[str] = []
        changed.append(self.__target)
        return changed


class Lanes:
    """
    Contains the definition of the Lanes that make up an Array Trap.
    """

    def __init__(self, distributor: Distributor, collector: Optional[Collector] = None):
        """
        Creates an instance of this class.
        """
        self.__distributor = distributor
        self.__collector = collector

    def get_count(self, tray: Tray) -> int:
        """
        Returns the number of lanes that should be created by an Array Trap.
        """
        return self.__distributor.get_count(tray)

    def pick_lane_data(self, index: int, tray: Tray) -> Dict[str, Any]:
        """
        Returns the data to be loaded into the Lane scope
        """
        return self.__distributor.pick_lane_data(index, tray)

    def put_lane_data(self, index: int, tray: Tray) -> List[str]:
        """
        Returns the data to be loaded into the Lane scope
        """
        if self.__collector is None:
            changed: List[str] = []
            return changed
        return self.__collector.put_lane_data(index, tray)
