"""
Contains the definition of a Marble class
"""

from typing import Callable, List, Optional

from .tray import Tray


class Marble:  # pylint: disable=too-few-public-methods
    """
    This class represents an object that is passed between and through Traps.
    """

    def __init__(
        self,
        track: "Track",  # type: ignore
        index: Optional[int] = None,
        restart_method: Callable[["Marble"], None] = None,  # type: ignore
        lanes: Optional[List[int]] = None,
    ):
        self.__index = index
        self.__lanes: Optional[List[int]] = lanes
        self.__restart_method = restart_method
        self.__track = track

    def converge(self, trap: "Downstream") -> Optional["Marble"]:  # type: ignore
        """
        Adds this Marble to the set of Marbles arriving at the supplied
        Downstream object. If set of Marbles is complete an new Marble
        will be created and returned while disposing of the Marbles in
        the set. If the set is not complete, then None is returned.
        """
        return self.__track.converge(trap, self)

    def dispose(self) -> None:
        """
        Disposes of this object.
        """
        return self.__track.dispose(self)

    def diverge(self, trap: "Upstream") -> List["Marble"]:  # type: ignore
        """
        Creates a set of Marbles that will replace this object passing
        out of the supplied Upstream object, while disposing of this
        object.
        """
        return self.__track.diverge(trap, self)

    def get_index(self) -> Optional[int]:
        """
        Returns the index, if any, of this object is a diverging set of Marbles.
        """
        return self.__index

    def get_lanes(self) -> Optional[List[int]]:
        """
        Returns the lanes associated with this objects current Trap.
        """
        return self.__lanes

    def get_storage(self) -> "TrackStorage":  # type: ignore
        """
        Returns the TrackStorage instance associated with this object.
        """
        return self.__track.get_storage()

    def get_tray(self) -> Tray:
        """
        Extracts the current workflow data items from this object.
        """
        return self.__track.get_tray()

    def is_terminated(self) -> Optional[bool]:
        """
        Returns True if this object belongs to a workflow that has terminated.
        """
        return self.__track.is_terminated()

    def restart(self) -> None:
        """
        Restart this Marble after it has been recovered.
        """
        self.__restart_method(self)

    def set_failure(self, failure: Exception) -> None:
        """
        Set the Exception that caused the current workflow to fail.
        """
        self.__track.set_failure(failure)

    def set_lanes(self, lanes: List[int]) -> None:
        """
        Set the lanes associated with this objects current Trap.
        """
        self.__lanes = lanes
