"""
Contains the definition of the MarbleFlowStorage interface
"""

# mypy: check-untyped-defs

from typing import Callable, List

from .marble import Marble
from .track import Track
from .track_storage import TrackStorage
from .trap import Trap


class MarbleFlowStorage:
    """
    This class contain the design from which Marble Flows may be checkpointed.
    """

    #    def __init__(self):
    #        """
    #        Creates an instance of this class.
    #        """
    #        self.__track_stores: List[TrackStorage] = []

    def add_track(self, track: Track) -> TrackStorage:
        """
        Adds a new track into the storage.
        """
        raise ValueError("This method must of overridden")

    def add_start(
        self, trap: Trap, downstream_method: Callable[[Marble], None], identity: str
    ) -> None:
        """
        Adds Start Trap details to this object.
        """
        raise ValueError("This method must of overridden")

    def add_trap(  # pylint: disable=too-many-arguments
        self,
        trap: Trap,
        downstream_method: Callable[[Marble], None],
        identity: str,
        indexed: bool = False,
        arrayed: bool = False,
    ) -> None:
        """
        Adds Trap (other than Start) details to this object.
        """
        raise ValueError("This method must of overridden")

    def recover_tracks(self) -> List[Track]:
        """
        Returns the collection of track recovered from the supplied store_location.
        """
        raise ValueError("This method must of overridden")
