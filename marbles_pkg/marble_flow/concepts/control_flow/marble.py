"""
Contains the definition of a Marble class
"""

from typing import List, Optional


class Marble:  # pylint: disable=too-few-public-methods
    """
    This class represents an object that is passed between and through ChannelGroups.
    """

    def __init__(self, track: "Track"):  # type: ignore
        self.__track = track

    def converge(self, trap: "Downstream") -> Optional["Marble"]:  # type: ignore
        """
        Adds this Marble to the set of Marbles arriving at the supplied
        Downstream object. If set of Marbles is complete an new Marble
        will be created and returned while disposing of the Marbles in
        the set. If the set is not complete, then None is returned.
        """
        return self.__track.converge(trap, self)

    def dispose(self) -> None:
        """
        Disposes of this object.
        """
        return self.__track.dispose(self)

    def diverge(self, trap: "Upstream") -> List["Marble"]:  # type: ignore
        """
        Creates a set of Marbles that will replace this object passing
        out of the supplied Upstream object, while disposing of this
        object.
        """
        return self.__track.diverge(trap, self)
