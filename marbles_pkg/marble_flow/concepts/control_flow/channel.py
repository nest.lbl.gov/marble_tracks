"""
Contains the definition of a Channel class
"""

from typing import Optional

from .marble import Marble
from .downstream import Downstream


class Channel:
    """
    This class represents a link between two Trap classes.
    """

    def __init__(self, downstream: Downstream):
        self.__downstream = downstream
        self.__downstream.add_incoming_channel(self)

    def evaluate_guard(self, _: Marble) -> Optional[bool]:
        """
        Returns True if the marble should be output via this object.
        """
        return None

    def transfer_marble(self, marble: Marble) -> None:
        """
        Transfers a Marble from the Upstream trap to the Downstream one.
        """
        self.__downstream.receive_marble(marble)
