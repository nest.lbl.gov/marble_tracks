"""
Contains the definition of a WrappedAction class
"""

from typing import Any, Callable, Dict, List, Optional

import logging

from .binding import Binding
from .lanes import Lanes
from .tray import Tray

Action = Callable[[Dict[str, Any]], Dict[str, Any]]
ScriptableAction = Callable[[List[str], Dict[str, Any]], Dict[str, Any]]


class WrappedAction:
    """
    This class wraps a Callable object with its data picker and putter.
    """

    def __init__(
        self,
        action: Action,
        picker_bindings: Optional[List[Binding]] = None,
        putter_bindings: Optional[List[Binding]] = None,
        lanes: Optional[Lanes] = None,
    ):
        """
        Creates an instance of this class.
        """
        self.__action = action
        self.__lanes = lanes
        if picker_bindings is None:
            self.__picker: List[Binding] = []
        else:
            self.__picker = picker_bindings
        if putter_bindings is None:
            self.__putter: List[Binding] = []
        else:
            self.__putter = putter_bindings

    def get_lane_count(self, tray: Tray) -> int:
        """
        Returns the number of lanes that should be created by an Array Trap.
        """
        # -1 means that this is not an Array Trap.
        if self.__lanes is None:
            return -1
        return self.__lanes.get_count(tray)

    def perform(self, tray: Tray) -> None:
        """
        Performs this object's action using the supplied Tray and
        putting the results o0f the action back into the Tray.
        """
        if self.__picker is None:
            arguments: Dict[str, Any] = {}
        else:
            arguments = tray.pick(self.__picker)
        logging.debug("Performing WrappedAction %s", self.__action)
        results = self.__action(arguments)
        logging.debug(
            "Results from WrappedAction %s are:\n    %s", self.__action, results
        )
        if self.__putter is not None:
            tray.put(self.__putter, results)

    def perform_lane(self, tray: Tray, index: int) -> None:
        """
        Performs this object's action within a lane adding a Lane scope
        to the supplied supplied Tray and putting the results in that
        scope back into the Tray.
        """
        if self.__lanes is None:
            raise ValueError("Should never reach here")
        lane_tray = Tray(tray)
        lane_tray.load(self.__lanes.pick_lane_data(index, tray))
        self.perform(lane_tray)
        self.__lanes.put_lane_data(index, lane_tray)
