"""
Contains the definition of a LanguageFactory class
"""

from typing import List

from .binding import Transformation
from .channel import Guard
from .wrapped_action import Action


class LanguageFactory:
    """
    This class is used to create executable elements of a Workflow by
    binding a language harness to a script.
    """

    def create_action(self, script: List[str]) -> Action:
        """
        Creates a Action from the supplied script.
        """
        raise ValueError("Must be overridden")

    def create_guard(self, script: List[str]) -> Guard:
        """
        Creates a Guard from the supplied script.
        """
        raise ValueError("Must be overridden")

    def create_transformation(self, script: List[str]) -> Transformation:
        """
        Creates a Transformation from the supplied script.
        """
        raise ValueError("Must be overridden")
