"""
Contains the definitions of a thread safe classes
"""

from threading import Lock


class Counter:
    """
    This class is a thread-safe integer that cna be incremented and
    decremented.
    """

    def __init__(self, value: int = 0):
        """
        Creates an instance of this class
        """
        self.__counter = value
        self.__lock = Lock()

    def increment(self, value: int = 1) -> int:
        """
        Increments the counter
        """
        with self.__lock:
            self.__counter += value
            return self.__counter

    def decrement(self, value: int = 1) -> int:
        """
        Decrements the counter
        """
        with self.__lock:
            self.__counter -= value
            return self.__counter

    def get_value(self) -> int:
        """
        Returns the current value of this object
        """
        with self.__lock:
            return self.__counter
