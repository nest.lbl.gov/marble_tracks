"""
Contains the definition of a Downstream class
"""

from .marble import Marble


class Downstream:
    """
    This class represents the object that is at the downstream end of on
    or more channels.
    """

    def add_incoming_channel(self, channel: "Channel") -> None:  # type: ignore
        """
        Adds an incoming Channel to this object.
        """

    def receive_marble(self, marble: Marble) -> None:
        """
        Receives the Marble to one or more Channels.
        """

    def incoming_size(self) -> int:
        """
        Returns the number of incoming Channels.
        """
        return 0
