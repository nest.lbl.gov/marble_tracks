"""
Contains the definition of a Track class
"""

from typing import Any, Dict, List, Optional

from threading import Thread
import traceback
import logging

from .marble import Marble
from .downstream import Downstream
from .tray import Tray
from .upstream import Upstream


class Track:
    """
    This class represents a concrete instance of the flow of control for
    a workflow defined in a Diagram.
    """

    def __init__(self, start: Upstream):
        """
        Creates an instance of this class.
        """
        self.__convergencies: Dict[Downstream, List[Marble]] = {}
        self.__failure: Optional[Exception] = None
        self.__marbles: List[Marble] = []
        self.__terminated: Optional[bool] = None
        self.__thread = Thread(target=self.__run)
        self.__tray = Tray()
        logging.debug("Created Tray %s", self.__tray)
        self.__upstream = start

    def converge(self, trap: Downstream, marble: Marble) -> Optional[Marble]:
        """
        Adds this Marble to the set of Marbles arriving at the supplied
        Downstream object. If set of Marbles is complete an new Marble
        will be created and returned while disposing of the Marbles in
        the set. If the set is not complete, then None is returned.
        """
        if trap not in self.__convergencies:
            current: List[Marble] = []
            self.__convergencies[trap] = current
        else:
            current = self.__convergencies[trap]
        if marble in current:
            raise ValueError("This Marble has already be handled by this Trap")
        if len(current) != (trap.incoming_size() - 1):
            # If wait for more arriving Marble, as this to list ans do nothing more.
            current.append(marble)
            return None
        # All Marbles have arrived, so execute convergence.
        result = self.__create_marble()
        for converging in current:
            self.__destroy_marble(converging)
        current.clear()
        self.__destroy_marble(marble)
        return result

    def __create_marble(self) -> Marble:
        """
        Create as new Marble in this object
        """
        marble = Marble(self)
        self.__marbles.append(marble)
        logging.debug("Created Marble %s", marble)
        return marble

    def __destroy_marble(self, marble: Marble) -> None:
        """
        Removes the supplied Marble from this object
        """
        try:
            self.__marbles.remove(marble)
            logging.debug("Destroyed Marble %s", marble)
        except ValueError:
            raise ValueError(  # pylint: disable=raise-missing-from
                "This Marble is not currenlty part of this Track"
            )

    def dispose(self, marble: Marble) -> None:
        """
        Disposes of the supplied Marble.
        """
        self.__destroy_marble(marble)

    def diverge(self, trap: Upstream, marble: Marble) -> List[Marble]:
        """
        Creates a set of Marbles that will replace this object passing
        out of the supplied Upstream object, while disposing of this
        object.
        """
        result: List[Marble] = []
        for _ in range(trap.outgoing_size()):
            result.append(self.__create_marble())
        self.__destroy_marble(marble)
        return result

    def extract(self) -> Optional[Dict[str, Any]]:
        """
        Extracts the current workflow data items from this object.
        """
        return self.__tray.extract()

    def get_tray(self) -> Tray:
        """
        Returns the Tray holding the workflow data item for this object.
        """
        return self.__tray

    def is_failure(self, timeout: Optional[float] = None) -> Optional[Exception]:
        """
        Blocks until this object has terminated, returning the
        Exception, if any, that cause it to fail.
        """
        self.__thread.join(timeout)
        return self.__failure

    def __run(self) -> None:
        """
        Moves the Marbles this object manages through the Diagram.
        """
        self.__terminated = False
        try:
            marble = self.__create_marble()
            self.__upstream.send_marble(marble)
            if 0 != len(self.__marbles):
                raise ValueError("Marbles still exist in Workflow")
        except Exception as failure:  # pylint: disable=broad-exception-caught
            stack = traceback.format_exc()
            self.__failure = failure
            logging.error(stack)
        self.__terminated = True
        logging.debug("Track terminated")

    def start(self, initial_data: Optional[Dict[str, Any]] = None) -> None:
        """
        Create an initial marble and starts it going though this object.
        """
        self.__tray.load(initial_data)
        self.__thread.start()

    def is_successful(self, timeout: Optional[float] = None) -> Optional[bool]:
        """
        Blocks until this object has terminated, returning True if it
        has terminated successfully.
        """
        self.__thread.join(timeout)
        if None is not self.__terminated:
            return None is self.__failure
        return None

    def is_terminated(self) -> Optional[bool]:
        """
        Returns True if this object has terminated,  either by completing or
        throwing an exception.
        """
        return self.__terminated
