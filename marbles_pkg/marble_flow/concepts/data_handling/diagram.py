"""
Contains the definition of a Diagram class
"""

# mypy: check-untyped-defs

from typing import Any, Callable, Dict, List, Optional

import logging

from .binding import Binding
from .tray import Tray
from .wrapped_action import Action, WrappedAction

Guard = Callable[[Dict[str, Any]], bool]


class Diagram:  # pylint: disable=too-few-public-methods
    """
    This class contain the design from which Tray objects can be created.
    """

    def __init__(self):
        """
        Creates an instance of this class.
        """
        self.__guards: List[Guard] = []
        self.__wrapped_actions: List[WrappedAction] = []

    def add_action(
        self,
        action: Action,
        picker_bindings: Optional[List[Binding]] = None,
        putter_bindings: Optional[List[Binding]] = None,
    ) -> None:
        """
        Adds a WrappedAction to this object.
        """
        result = WrappedAction(action, picker_bindings, putter_bindings)
        logging.debug("Created WrappedWaction %s", result)
        self.__wrapped_actions.append(result)

    def add_guard(self, guard: Guard) -> None:
        """
        Adds a Guard to this object.
        """
        logging.debug("Adding Guard %s", guard)
        self.__guards.append(guard)

    def create_tray(self) -> Tray:
        """
        Create a new Tray instance.
        """
        result = Tray()
        logging.debug("Created Tray %s", result)
        return result

    def get_actions(self) -> List[WrappedAction]:
        """
        Returns the collection of WrappedAction instances contained in
        this object.
        """
        return self.__wrapped_actions

    def get_guards(self) -> List[Guard]:
        """
        Returns the collection of Guard instances contained in
        this object.
        """
        return self.__guards
