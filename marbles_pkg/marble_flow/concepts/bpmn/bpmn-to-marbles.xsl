<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL"
    xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
    version="1.0">
  <xsl:output method="text" /> 
  <!-- declare newline variable -->
  <xsl:variable name="newline">
    <xsl:text>
</xsl:text>
  </xsl:variable>

  <xsl:template match="bpmn2:operation">
      <xsl:param name="process"/>
[[<xsl:value-of select="$process"/>.external.action]]
id = "<xsl:value-of select="@id" />"
operation = "split string"
  </xsl:template>

  <xsl:template match="bpmn2:interface">
      <xsl:param name="process"/>
      <xsl:apply-templates select="bpmn2:operation" >
          <xsl:with-param name="process" select="$process" />
      </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="bpmn2:dataInputAssociation" mode="flavor">
    <xsl:text>picker</xsl:text>
  </xsl:template>

  <xsl:template match="bpmn2:dataOutputAssociation" mode="flavor">
    <xsl:text>putter</xsl:text>
  </xsl:template>

  <xsl:template match="bpmn2:dataInputAssociation | bpmn2:dataOutputAssociation" mode="data">
      <xsl:param name="process"/>
[[<xsl:value-of select="$process"/>.data.<xsl:apply-templates select="." mode="flavor"/>]]
id = "<xsl:value-of select="@id" />"
source = "<xsl:value-of select="bpmn2:sourceRef" />"
target = "<xsl:value-of select="bpmn2:targetRef" />"
  </xsl:template>

  <xsl:template match="bpmn2:serviceTask" mode="data">
      <xsl:param name="process"/>
      <xsl:apply-templates select="bpmn2:dataInputAssociation | bpmn2:dataOutputAssociation"  mode="data">
          <xsl:with-param name="process" select="$process" />
      </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="bpmn2:startEvent" mode="flavor">
    <xsl:text>start</xsl:text>
  </xsl:template>

  <xsl:template match="bpmn2:endEvent" mode="flavor">
    <xsl:text>end</xsl:text>
  </xsl:template>

  <xsl:template match="bpmn2:startEvent | bpmn2:endEvent">
      <xsl:param name="process"/>
[[<xsl:value-of select="$process"/>.control.<xsl:apply-templates select="." mode="flavor"/>]]
id = "<xsl:value-of select="@id" />"
  </xsl:template>

  <xsl:template match="bpmn2:dataInputAssociation | bpmn2:dataOutputAssociation">
      <xsl:param name="process"/>
<xsl:text>"</xsl:text><xsl:value-of select="@id" /><xsl:text>",</xsl:text>
  </xsl:template>

  <xsl:template match="bpmn2:serviceTask">
      <xsl:param name="process"/>
[[<xsl:value-of select="$process"/>.control.action]]
id = "<xsl:value-of select="@id" />"
action = "<xsl:value-of select="@operationRef" />"
pickers = [<xsl:apply-templates select="bpmn2:dataInputAssociation" />]
putters = [<xsl:apply-templates select="bpmn2:dataOutputAssociation" />]
  </xsl:template>

   <xsl:template match="bpmn2:sequenceFlow">
      <xsl:param name="process"/>
[[<xsl:value-of select="$process"/>.control.channel]]
upstream = "<xsl:value-of select="@sourceRef" />"
downstream = "<xsl:value-of select="@targetRef" />"
  </xsl:template>

  <xsl:template match="bpmn2:process">
[<xsl:value-of select="@id"/>.spec]
id = "<xsl:value-of select="@id" />"
      <xsl:apply-templates select="../bpmn2:interface">
          <xsl:with-param name="process" select="@id" />
      </xsl:apply-templates>
      <xsl:apply-templates select="bpmn2:serviceTask" mode="data">
          <xsl:with-param name="process" select="@id" />
      </xsl:apply-templates>
      <xsl:apply-templates select="bpmn2:startEvent">
          <xsl:with-param name="process" select="@id" />
      </xsl:apply-templates>
      <xsl:apply-templates select="bpmn2:serviceTask">
          <xsl:with-param name="process" select="@id" />
      </xsl:apply-templates>
      <xsl:apply-templates select="bpmn2:endEvent">
          <xsl:with-param name="process" select="@id" />
      </xsl:apply-templates>
      <xsl:apply-templates select="bpmn2:sequenceFlow">
          <xsl:with-param name="process" select="@id" />
      </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="bpmn2:definitions">
      <xsl:apply-templates select="bpmn2:process" />
      <xsl:value-of select="$newline"/>
  </xsl:template>

</xsl:stylesheet>
