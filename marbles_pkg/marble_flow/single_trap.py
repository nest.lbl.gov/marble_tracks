"""
Contains the construction and execution of a Workflow that is a
diverging and converging pair of Exclusive Traps.
"""

# mypy: disable-error-code=method-assign

from typing import Optional

import logging
import os
from types import MethodType


from marble_flow.concepts.control_flow import Diagram


def announce(__, _):
    """
    Prints out that this Trap is performing its action.
    """
    logging.info("A marble is passing through the single Trap")


def create_diagram() -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    end = diagram.add_end()
    trap = diagram.add_action_trap()
    trap.perform_action = MethodType(announce, trap)
    diagram.connect(trap, end)
    start = diagram.add_start()
    diagram.connect(start, trap)
    return diagram


def execute() -> Optional[bool]:
    """
    Runs this flow of control.
    """
    diagram = create_diagram()
    track = diagram.create_track()
    track.start()
    return track.is_successful()


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    success = execute()
    if success is None:
        logging.info("Flow of control is imcomplete")
        return 2
    if not success:
        logging.info("Flow of control failed")
        return 1
    logging.info("Flow of control succeeded")
    return 0


if __name__ == "__main__":
    main()
