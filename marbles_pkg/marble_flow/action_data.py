"""
Contains the construction and execution of a Workflow that just has a
start and end and nothing else.
"""

from typing import Any, Dict, List, Optional

import logging
import os


from marble_flow.concepts.data_handling import Action, Binding, Diagram

DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]
INITIAL_DATA = {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "two"}
EXPECTED = [
    {DATA_IDENTITIES[0]: 2, DATA_IDENTITIES[1]: "two"},
    {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "three"},
]

ENGLISH_NUMBERS = ["zero", "one", "two", "three"]


def english_to_int(value: str) -> int:
    """
    Changes the English word into its integer.
    """
    return ENGLISH_NUMBERS.index(value)


def int_to_english(value: int) -> str:
    """
    Changes an integer into its English word.
    """
    return ENGLISH_NUMBERS[value]


PICKER_BINDINGS = [
    [Binding(DATA_IDENTITIES[0], "input")],
    [Binding(DATA_IDENTITIES[1], "input", english_to_int)],
]
PUTTER_BINDINGS = [
    [Binding("output", DATA_IDENTITIES[0])],
    [Binding("output", DATA_IDENTITIES[1], int_to_english)],
]


def add_one(args):
    """
    Adds 1 to the argument named "input"
    """
    results = {}
    results["output"] = args["input"] + 1
    return results


def create_diagram(
    action: Action, picker: List[Binding], putter: List[Binding]
) -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    diagram.add_action(action, picker, putter)
    return diagram


def execute(
    action: Action,
    picker: List[Binding],
    putter: List[Binding],
    initial_data: Optional[Dict[str, Any]] = None,
) -> Optional[Dict[str, Any]]:
    """
    Runs this data handling.
    """
    diagram = create_diagram(action, picker, putter)
    tray = diagram.create_tray()
    tray.load(initial_data)
    for wrapped_action in diagram.get_actions():
        wrapped_action.perform(tray)
    return tray.extract()


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    for example in range(0, 2):
        results = execute(
            add_one, PICKER_BINDINGS[example], PUTTER_BINDINGS[example], INITIAL_DATA
        )
        if results is None:
            logging.info("Data handling is incomplete")
            return 2
        for key in results.keys():
            if (
                EXPECTED[example][key] != results[key]
            ):  # pylint: disable=unsubscriptable-object
                logging.info("Data handling failed")
                return 1
    logging.info("Data handling succeeded")
    return 0


if __name__ == "__main__":
    main()
