"""
Contains the construction and execution of a Workflow that just has a
start and end and nothing else.
"""

from typing import Optional

import logging
import os


from marble_flow.concepts.control_flow import Diagram


def create_diagram() -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    end = diagram.add_end()
    start = diagram.add_start()
    diagram.connect(start, end)
    return diagram


def execute() -> Optional[bool]:
    """
    Runs this flow of control.
    """
    diagram = create_diagram()
    track = diagram.create_track()
    track.start()
    return track.is_successful()


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    success = execute()
    if success is None:
        logging.info("Flow of control is imcomplete")
        return 2
    if not success:
        logging.info("Flow of control failed")
        return 1
    logging.info("Flow of control succeeded")
    return 0


if __name__ == "__main__":
    main()
