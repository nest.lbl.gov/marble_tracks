"""
Contains the construction and execution of a Workflow that is a single
Action Trap.
"""

from typing import Any, Dict, Optional, Tuple

import logging
import os
import sys

from tomlkit.toml_file import TOMLFile


from marble_flow.concepts.array_traps import (
    build_diagram,
    ExternalsFactory,
)


def split_string(args):
    """
    Splited in argument "input" into words that are returned in "output"
    """
    results = {}
    results["output_words"] = args["input_string"].split()
    return results


DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]

INITIAL_DATA = {
    DATA_IDENTITIES[0]: ["two words", "three short words"],
    DATA_IDENTITIES[1]: [None, None],
}
EXPECTED = {
    DATA_IDENTITIES[0]: ["two words", "three short words"],
    DATA_IDENTITIES[1]: [["two", "words"], ["three", "short", "words"]],
}


def execute(
    input_file: TOMLFile,
    workflow: str,
    externals_factory: ExternalsFactory,
    initial_data: Dict[str, Any],
) -> Optional[Dict[str, Any]]:
    """
    Runs this flow of control.
    """
    document = input_file.read()
    diagram = build_diagram(document[workflow], externals_factory)
    track = diagram.create_track()
    track.start(initial_data)
    if track.is_successful():
        return track.extract()
    return None


def parse_args() -> Tuple[str, str]:
    """
    Parses the command line arguments
    """
    arg_count = len(sys.argv)
    if arg_count < 3:
        workflow_name = "arrayed_action"
    else:
        workflow_name = sys.argv[2]
    if arg_count < 2:
        workflow_definition = (
            "marbles_pkg/marble_flow/concepts/array_traps/arrayed_action.toml"
        )
    else:
        workflow_definition = sys.argv[1]
    return workflow_definition, workflow_name


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)

    workflow_definition, workflow_name = parse_args()

    externals_factory = ExternalsFactory()
    externals_factory.add_action("split string", split_string)

    results = execute(
        TOMLFile(workflow_definition),
        workflow_name,
        externals_factory,
        INITIAL_DATA,
    )

    if "arrayed_action" == workflow_name:
        if results is None:
            logging.info("Workflow is incomplete or failed")
            return 2
        for key in results.keys():
            if EXPECTED[key] != results[key]:  # pylint: disable=unsubscriptable-object
                logging.info("Workflow failed")
                return 1
    logging.info("Workflows succeeded")
    return 0


if __name__ == "__main__":
    main()
