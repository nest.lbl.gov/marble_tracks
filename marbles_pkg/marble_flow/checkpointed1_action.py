"""
Contains the construction and execution of a Workflow that is a single
Action Trap.
"""

# mypy: check-untyped-defs

from typing import cast, Any, Dict, Optional, Tuple

from concurrent.futures import ThreadPoolExecutor
import logging
import os
from pathlib import Path
import sys

from tomlkit.toml_file import TOMLFile


from marble_flow.concepts.checkpointing1 import (
    build_diagram,
    ExternalsFactory,
    TerminationCallback,
    Track,
    WorkflowExecutor,
    XmlStorageBuilder,
)


def split_string(args):
    """
    Splited in argument "input" into words that are returned in "output"
    """
    results = {}
    results["output_words"] = args["input_string"].split()
    return results


def north_announce(_):
    """
    Prints out that the North Trap is performing its action.
    """
    results = {}
    results["result"] = "north"
    logging.info("A marble is passing through north Trap")
    return results


def south_announce(_):
    """
    Prints out that the South Trap is performing its action.
    """
    logging.info("A marble is passing through south Trap")


DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]

WORKFLOW_EXECUTOR = WorkflowExecutor(ThreadPoolExecutor(), 1)

INITIAL_DATA = {DATA_IDENTITIES[0]: "two words", DATA_IDENTITIES[1]: None}
EXPECTED = [
    INITIAL_DATA,
    {DATA_IDENTITIES[0]: "two words", DATA_IDENTITIES[1]: ["two", "words"]},
    {DATA_IDENTITIES[0]: "north", DATA_IDENTITIES[1]: None},
]


class CollectValues(TerminationCallback):
    """
    TerminationCallback implementation that collects the final workflow
    data items from each Track.
    """

    def __init__(self):
        """
        Creates and instance of this class.
        """
        self.__values: Dict[str, Optional[Dict[str, Any]]] = {}

    def terminated(self, track: Track, failure: Optional[Exception]) -> None:
        """
        Called when a Track terminates.
        """
        label = track.get_label()
        if label is not None:
            self.__values[label] = track.extract()

    def get_values(self) -> Dict[str, Optional[Dict[str, Any]]]:
        """
        Returns the final workflow data items
        """
        return self.__values


def execute(
    input_file: TOMLFile,
    workflow: str,
    externals_factory: ExternalsFactory,
    workflow_executor: WorkflowExecutor,
    recovery_file: str,
) -> Optional[Dict[str, Any]]:
    """
    Runs this flow of control.
    """
    document = input_file.read()
    termination_callback = CollectValues()
    diagram = build_diagram(
        document[workflow],
        externals_factory,
        workflow_executor,
        termination_callback,
        XmlStorageBuilder(Path(recovery_file)),
    )
    recovered_tracks = diagram.recover_tracks()
    if recovered_tracks is None or 0 == len(recovered_tracks):
        track = diagram.create_track("Label")
        track.start(INITIAL_DATA)
    else:
        track = recovered_tracks[0]
        track.restart()
    if track.is_successful():
        return track.extract()
    return None


def parse_args() -> Tuple[str, str, str]:
    """
    Parses the command line arguments
    """
    arg_count = len(sys.argv)
    if arg_count < 4:
        workflow_name = "start_end"
    else:
        workflow_name = sys.argv[3]
    if arg_count < 3:
        workflow_definition = (
            "marbles_pkg/marble_flow/concepts"
            + "/checkpointing1/examples/start_end/start_end.toml"
        )
    else:
        workflow_definition = sys.argv[2]
    if arg_count < 2:
        recovery_file = (
            "marbles_pkg/marble_flow/concepts/"
            + "checkpointing1/examples/start_end/created.xml"
        )
    else:
        recovery_file = sys.argv[1]
    return workflow_definition, workflow_name, recovery_file


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)

    workflow_definition, workflow_name, recovery_file = parse_args()

    externals_factory = ExternalsFactory()
    if "single_action" == workflow_name:
        externals_factory.add_action("split string", split_string)
    if "parallel_actions" == workflow_name:
        externals_factory.add_action("north announce", north_announce)
        externals_factory.add_action("south announce", south_announce)

    results = execute(
        TOMLFile(workflow_definition),
        workflow_name,
        externals_factory,
        WORKFLOW_EXECUTOR,
        recovery_file,
    )

    if results is None:
        logging.info("Workflow is incomplete or failed")
        return 2
    if "start_end" == workflow_name:
        expected_index = 0
    elif "single_action" == workflow_name:
        expected_index = 1
    else:
        expected_index = 2
    for key in results.keys():
        if cast(dict, EXPECTED[expected_index])[key] != results[key]:
            logging.info("Workflow failed")
            return 1
    logging.info("Workflows succeeded")
    return 0


if __name__ == "__main__":
    main()
