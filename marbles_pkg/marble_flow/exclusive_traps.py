"""
Contains the construction and execution of a Workflow that is a
diverging and converging pair of Exclusive Traps.
"""

# mypy: disable-error-code=method-assign

from typing import Optional

import logging
import os
from types import MethodType


from marble_flow.concepts.control_flow import Diagram


def north_announce(__, _):
    """
    Prints out that the North Trap is performing its action.
    """
    logging.info("A marble is passing through north Trap")


def south_announce(__, _):
    """
    Prints out that the South Trap is performing its action.
    """
    logging.info("A marble is passing through south Trap")


def selected_channel(__, _):
    """
    Returns True for a Channel's guard condition.
    """
    return True


def ignore_channel(__, _):
    """
    Returns False for a Channel's guard condition.
    """
    return False


def create_diagram(north_bound: bool) -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    end = diagram.add_end()
    merging = diagram.add_downstream_trap()
    merging.set_exclusive_mode(True)
    diagram.connect(merging, end)
    north = diagram.add_action_trap()
    north.perform_action = MethodType(north_announce, north)
    diagram.connect(north, merging)
    south = diagram.add_action_trap()
    south.perform_action = MethodType(south_announce, south)
    diagram.connect(south, merging)
    splitting = diagram.add_upstream_trap()
    splitting.set_exclusive_mode(True)
    channel = diagram.connect(splitting, north)
    if north_bound:
        channel.evaluate_guard = MethodType(selected_channel, channel)
    else:
        channel.evaluate_guard = MethodType(ignore_channel, channel)
    diagram.connect(splitting, south)
    start = diagram.add_start()
    diagram.connect(start, splitting)
    return diagram


def execute(north_bound: bool) -> Optional[bool]:
    """
    Runs this flow of control.
    """
    diagram = create_diagram(north_bound)
    track = diagram.create_track()
    track.start()
    return track.is_successful()


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    for north_bound in [True, False]:
        success = execute(north_bound)
        if success is None:
            logging.info("Flow of control is imcomplete")
            return 2
        if not success:
            logging.info("Flow of control failed")
            return 1
        logging.info("Flow of control succeeded")
    return 0


if __name__ == "__main__":
    main()
