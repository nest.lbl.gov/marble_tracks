"""
Contains the construction and execution of a Workflow that is a
diverging and converging pair of Exclusive Traps.
"""

# mypy: disable-error-code=method-assign

from typing import Optional

import logging
import os
from types import MethodType


from marble_flow.concepts.control_flow import Diagram


def north_announce(__, _):
    """
    Prints out that the North Trap is performing its action.
    """
    logging.info("A marble is passing through north Trap")


def south_announce(__, _):
    """
    Prints out that the South Trap is performing its action.
    """
    logging.info("A marble is passing through south Trap")


def selected_channel(__, _):
    """
    Returns True for a Channel's guard condition.
    """
    return True


def create_diagram() -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    end = diagram.add_end()
    joining = diagram.add_downstream_trap()
    joining.set_exclusive_mode(False)
    diagram.connect(joining, end)
    north = diagram.add_action_trap()
    north.perform_action = MethodType(north_announce, north)
    diagram.connect(north, joining)
    south = diagram.add_action_trap()
    south.perform_action = MethodType(south_announce, south)
    diagram.connect(south, joining)
    forking = diagram.add_upstream_trap()
    forking.set_exclusive_mode(False)
    diagram.connect(forking, north)
    diagram.connect(forking, south)
    start = diagram.add_start()
    diagram.connect(start, forking)
    return diagram


def execute() -> Optional[bool]:
    """
    Runs this flow of control.
    """
    diagram = create_diagram()
    track = diagram.create_track()
    track.start()
    return track.is_successful()


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    success = execute()
    if success is None:
        logging.info("Flow of control is imcomplete")
        return 2
    if not success:
        logging.info("Flow of control failed")
        return 1
    logging.info("Flow of control succeeded")
    return 0


if __name__ == "__main__":
    main()
