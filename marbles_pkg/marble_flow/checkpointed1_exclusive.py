"""
Contains the construction and execution of a Workflow that is a
diverging and converging pair of Exclusive Traps.
"""

# mypy: check-untyped-defs

from typing import Any, Dict, List, Optional, Tuple

from concurrent.futures import ThreadPoolExecutor
import logging
import os
import sys
import time

from tomlkit.toml_file import TOMLFile


from marble_flow.concepts.threading import (
    build_diagram,
    ExternalsFactory,
    Track,
    WorkflowExecutor,
)

GUARD_NAMES = [
    "english",
    "integer",
]


ACTION_SLEEP = 0.5


def add_one(args):
    """
    Adds 1 to the argument workflowd "input"
    """
    logging.debug("Sleeping for %.1f seconds", ACTION_SLEEP)
    time.sleep(ACTION_SLEEP)
    results = {}
    results["output"] = args["input"] + 1
    logging.debug("Action completed")
    return results


ENGLISH_NUMBERS = ["zero", "one", "two", "three"]


def english_to_int(value: str) -> int:
    """
    Changes the English word into its integer.
    """
    return ENGLISH_NUMBERS.index(value)


def int_to_english(value: int) -> str:
    """
    Changes an integer into its English word.
    """
    return ENGLISH_NUMBERS[value]


DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]


def equals_two_integer(workflow_data: Dict[str, Any]) -> bool:
    """
    True if DATA_IDENTITIES[0] is the numeral 1.
    """
    return workflow_data[DATA_IDENTITIES[0]] == 2


def equals_two_english(workflow_data: Dict[str, Any]) -> bool:
    """
    True if DATA_IDENTITIES[1] is the English word "two"
    """
    return workflow_data[DATA_IDENTITIES[1]] == "two"


WORKFLOW_EXECUTOR = WorkflowExecutor(ThreadPoolExecutor(), 1)

INITIAL_DATA = [
    {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "two"},
    {DATA_IDENTITIES[0]: 2, DATA_IDENTITIES[1]: "one"},
]
EXPECTED = [
    [
        {DATA_IDENTITIES[0]: 2, DATA_IDENTITIES[1]: "two"},
        {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "three"},
    ],
    [
        {DATA_IDENTITIES[0]: 2, DATA_IDENTITIES[1]: "two"},
        {DATA_IDENTITIES[0]: 3, DATA_IDENTITIES[1]: "one"},
    ],
]
NORTH_BOUND = [True, False]


def execute(  # pylint: disable=too-many-arguments
    input_file: TOMLFile,
    workflow: str,
    externals_factory: ExternalsFactory,
    initial_data: List[Dict[str, Any]],
    workflow_executor: Optional[WorkflowExecutor] = None,
) -> Optional[List[Dict[str, Any]]]:
    """
    Runs this flow of control.
    """
    tracks: List[Track] = []
    document = input_file.read()
    diagram = build_diagram(document[workflow], externals_factory, workflow_executor)
    for initial in [0, 1]:
        tracks.append(diagram.create_track(str(len(tracks))))
        tracks[initial].start(initial_data[initial])
    results: List[Dict[str, Any]] = []
    for initial in [0, 1]:
        successful = tracks[initial].is_successful()
        while successful is None:
            time.sleep(0.1)
            successful = tracks[initial].is_successful()
        if successful:
            values = tracks[initial].extract()
            if values is not None:
                results.append(values)
        else:
            print(tracks[initial].is_failure())
    return results


def parse_args() -> Tuple[str, str]:
    """
    Parses the command line arguments
    """
    arg_count = len(sys.argv)
    if arg_count < 3:
        workflow_name = "exclusive_traps"
    else:
        workflow_name = sys.argv[2]
    if arg_count < 2:
        workflow_definition = (
            "marbles_pkg/marble_flow/concepts/definition/exclusive_traps.toml"
        )
    else:
        workflow_definition = sys.argv[1]
    return workflow_definition, workflow_name


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)

    workflow_definition, workflow_name = parse_args()

    if "exclusive_traps" == workflow_name:
        loops = 2
    else:
        loops = 1
    for example in range(0, loops):
        externals_factory = ExternalsFactory()
        externals_factory.add_action("add one", add_one)
        if 0 == example:
            externals_factory.add_guard("select_north", equals_two_english)
        else:
            externals_factory.add_guard("select_north", equals_two_integer)
        externals_factory.add_transformation("english_to_int", english_to_int)
        externals_factory.add_transformation("int_to_english", int_to_english)
        results = execute(
            TOMLFile(workflow_definition),
            workflow_name,
            externals_factory,
            INITIAL_DATA,
            WORKFLOW_EXECUTOR,
        )
        if "exclusive_traps" == workflow_name:
            if results is None:
                logging.info("Workflow is incomplete or failed")
                return 2
            for initial in [0, 1]:
                for key in results[initial].keys():
                    if (
                        EXPECTED[initial][example][key] != results[initial][key]
                    ):  # pylint: disable=unsubscriptable-object
                        logging.info("Workflow failed")
                        return 1
    logging.info("Workflows succeeded")
    return 0


if __name__ == "__main__":
    main()
