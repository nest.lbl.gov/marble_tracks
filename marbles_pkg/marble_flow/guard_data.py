"""
Contains the construction and execution of a Workflow that just has a
start and end and nothing else.
"""

from typing import Any, Dict, List, Optional

import logging
import os


from marble_flow.concepts.data_handling import Diagram, Guard

DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]
INITIAL_DATA = {DATA_IDENTITIES[0]: 1, DATA_IDENTITIES[1]: "two"}
EXPECTED = [
    [True],
    [False],
]

ENGLISH_NUMBERS = ["zero", "one", "two"]


def equals_two_integer(workflow_data: Dict[str, Any]) -> bool:
    """
    True if DATA_IDENTITIES[0] is the numeral 1.
    """
    return workflow_data[DATA_IDENTITIES[0]] == 2


def equals_two_english(workflow_data: Dict[str, Any]) -> bool:
    """
    True if DATA_IDENTITIES[1] is the English word "two"
    """
    return workflow_data[DATA_IDENTITIES[1]] == "two"


GUARDS = [
    equals_two_english,
    equals_two_integer,
]


def create_diagram(
    guard: Guard,
) -> Diagram:
    """
    Creates the Diagram to be used in this example
    """
    diagram = Diagram()
    diagram.add_guard(guard)
    return diagram


def execute(
    guard: Guard,
    initial_data: Optional[Dict[str, Any]] = None,
) -> List[bool]:
    """
    Runs this data handling.
    """
    diagram = create_diagram(guard)
    tray = diagram.create_tray()
    tray.load(initial_data)
    results: List[bool] = []
    workflow_data = tray.extract()
    for guard_condition in diagram.get_guards():
        if workflow_data is None:
            results.append(guard_condition({}))
        else:
            results.append(guard_condition(workflow_data))
    return results


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)
    for example in range(0, 2):
        results = execute(GUARDS[example], INITIAL_DATA)
        if results is None:
            logging.info("Data handling is incomplete")
            return 2
        index = 0
        for result in results:
            if EXPECTED[example][index] != result:
                logging.info("Data handling failed")
                return 1
            index = index + 1
    logging.info("Data handling succeeded")
    return 0


if __name__ == "__main__":
    main()
