"""
Contains the construction and execution of a Workflow that is a single
Action Trap.
"""

from typing import Any, Dict, Optional, Tuple

import logging
import os
import sys

from tomlkit.toml_file import TOMLFile


from marble_flow.concepts.array_traps import (
    build_diagram,
    ExternalsFactory,
)


def execute(
    input_file: TOMLFile,
    workflow: str,
    externals_factory: ExternalsFactory,
) -> Optional[Dict[str, Any]]:
    """
    Runs this flow of control.
    """
    document = input_file.read()
    diagram = build_diagram(  # pylint: disable=unused-variable
        document[workflow], externals_factory
    )
    return {}


def parse_args() -> Tuple[str, str]:
    """
    Parses the command line arguments
    """
    arg_count = len(sys.argv)
    if arg_count < 2:
        raise ValueError("Must Workflow File")
    if arg_count < 3:
        raise ValueError("Must provide name of Workflow")
    workflow_definition = sys.argv[1]
    workflow_name = sys.argv[2]
    return workflow_definition, workflow_name


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)

    workflow_definition, workflow_name = parse_args()

    externals_factory = ExternalsFactory()

    results = execute(  # pylint: disable=unused-variable
        TOMLFile(workflow_definition),
        workflow_name,
        externals_factory,
    )

    logging.info("Workflow validated")
    return 0


if __name__ == "__main__":
    main()
