"""
Contains the construction and execution of a Workflow that is a single
Action Trap.
"""

# mypy: check-untyped-defs

from typing import Any, Dict, Optional, Tuple

from concurrent.futures import ThreadPoolExecutor
import logging
import os
from pathlib import Path
import sys

from tomlkit.toml_file import TOMLFile


from marble_flow.concepts.checkpointing2 import (
    build_diagram,
    ExternalsFactory,
    TerminationCallback,
    Track,
    WorkflowExecutor,
    XmlStorageBuilder,
)


def split_string(args):
    """
    Splited in argument "input" into words that are returned in "output"
    """
    results = {}
    results["output_words"] = args["input_string"].split()
    return results


DATA_IDENTITIES = ["IDENTITY_1", "IDENTITY_2"]

WORKFLOW_EXECUTOR = WorkflowExecutor(ThreadPoolExecutor(), 1)

INITIAL_DATA = {
    DATA_IDENTITIES[0]: ["two words", "three short words"],
    DATA_IDENTITIES[1]: [None, None],
}
EXPECTED = {
    DATA_IDENTITIES[0]: ["two words", "three short words"],
    DATA_IDENTITIES[1]: [["two", "words"], ["three", "short", "words"]],
}


class CollectValues(TerminationCallback):
    """
    TerminationCallback implementation that collects the final workflow
    data items from each Track.
    """

    def __init__(self):
        """
        Creates and instance of this class.
        """
        self.__values: Dict[str, Optional[Dict[str, Any]]] = {}

    def terminated(self, track: Track, failure: Optional[Exception]) -> None:
        """
        Called when a Track terminates.
        """
        label = track.get_label()
        if label is not None:
            self.__values[label] = track.extract()

    def get_values(self) -> Dict[str, Optional[Dict[str, Any]]]:
        """
        Returns the final workflow data items
        """
        return self.__values


def execute(
    input_file: TOMLFile,
    workflow: str,
    externals_factory: ExternalsFactory,
    workflow_executor: WorkflowExecutor,
    recovery_file: str,
) -> Optional[Dict[str, Any]]:
    """
    Runs this flow of control.
    """
    document = input_file.read()
    termination_callback = CollectValues()
    diagram = build_diagram(
        document[workflow],
        externals_factory,
        workflow_executor,
        termination_callback,
        XmlStorageBuilder(Path(recovery_file)),
    )
    recovered_tracks = diagram.recover_tracks()
    if recovered_tracks is None or 0 == len(recovered_tracks):
        track = diagram.create_track("Label")
        track.start(INITIAL_DATA)
    else:
        track = recovered_tracks[0]
        track.restart()
    if track.is_successful():
        return track.extract()
    return None


def parse_args() -> Tuple[str, str, str]:
    """
    Parses the command line arguments
    """
    arg_count = len(sys.argv)
    if arg_count < 4:
        workflow_name = "arrayed_action"
    else:
        workflow_name = sys.argv[2]
    if arg_count < 3:
        workflow_definition = (
            "marbles_pkg/marble_flow/concepts"
            + "/checkpointing2/examples/arrayed_action/arrayed_action.toml"
        )
    else:
        workflow_definition = sys.argv[1]
    if arg_count < 2:
        recovery_file = (
            "marbles_pkg/marble_flow/concepts/"
            + "checkpointing2/examples/arrayed_action/created.xml"
        )
    else:
        recovery_file = sys.argv[1]
    return workflow_definition, workflow_name, recovery_file


def main() -> int:
    """
    Build and runs a simple start-end workflow.
    """
    if "LOG_LEVEL" in os.environ:
        log_level = getattr(logging, os.environ["LOG_LEVEL"].upper(), None)
    else:
        log_level = logging.INFO
    logging.basicConfig(level=log_level)

    workflow_definition, workflow_name, recovery_file = parse_args()

    externals_factory = ExternalsFactory()
    externals_factory.add_action("split string", split_string)

    results = execute(
        TOMLFile(workflow_definition),
        workflow_name,
        externals_factory,
        WORKFLOW_EXECUTOR,
        recovery_file,
    )

    if "arrayed_action" == workflow_name:
        if results is None:
            logging.info("Workflow is incomplete or failed")
            return 2
        for key in results.keys():
            if EXPECTED[key] != results[key]:  # pylint: disable=unsubscriptable-object
                logging.debug('Key was "%s", with value "%s"', key, str(results[key]))
                logging.info("Workflow failed")
                return 1
    logging.info("Workflows succeeded")
    return 0


if __name__ == "__main__":
    main()
